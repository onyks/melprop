/*
 * sensor_hub.h
 *
 *  Created on: 29.10.2016
 *      Author: krystian
 */

#ifndef SENSOR_HUB_H_
#define SENSOR_HUB_H_

#include "ch.h"

/* Exported definitions ---------------------------------------------------- */
/* Settings ---------------------------------------------------------------- */
/**
 * @brief Defines stack size of the Sensor Hub thread
 */
#define SENSOR_HUB_STACK_SIZE				(2048)

/**
 * @brief Defines priority of the Sensor Hub thread
 */
#define SENSOR_HUB_PRIORITY					(NORMALPRIO)

/* Events ------------------------------------------------------------------ */
/**
 * @brief Notifies Sensor Hub that new gyroscope data is ready
 */
#define SENSOR_HUB_EVT_GYRO_NEW_DATA		((eventmask_t)(1 << 0))

/**
 * @brief Notifies Sensor Hub that new accelerometer data is ready
 */
#define SENSOR_HUB_EVT_ACCEL_NEW_DATA		((eventmask_t)(1 << 1))

/**
 * @brief Notifies Sensor Hub that new magnetometer data is ready
 */
#define SENSOR_HUB_EVT_MAG_NEW_DATA			((eventmask_t)(1 << 2))

/**
 * @brief Notifies Sensor Hub that new data form the pressure sensor is ready
 */
#define SENSOR_HUB_EVT_PRESS_NEW_DATA		((eventmask_t)(1 << 3))


/* Exported functions ------------------------------------------------------ */
/**
 * Starts sensor hub thread.
 * @remark
 * Thread's stack size and priority are set using SENSOR_HUB_STACK_SIZE
 * and SENSOR_HUB_PRIORITY definitions respectively.
 */
void SensorHub_Start(void);

/**
 * Return pointer to the Sensor Hub thread
 */
thread_t* SensorHub_GetThreadPtr(void);

#endif /* SENSOR_HUB_H_ */
