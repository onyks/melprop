/*
 * sensor_hub.c
 *
 *  Created on: 29.10.2016
 *      Author: krystian
 */

#include "sensor_hub.h"
#include "shell_lib.h"
#include "vector.h"

#include "bmp280.h"
#include "bmp280_lld.h"
#include "lsm9ds0.h"
#include "lsm9ds0_lld.h"

/* Private variables ------------------------------------------------------- */
static thread_t *SensorHub_ThreadPtr = NULL;
static THD_WORKING_AREA(SensorHub_ThreadWA, SENSOR_HUB_STACK_SIZE);

/* Sensors */
static struct bmp280_t bmp280;
static lsm9ds0_t lsm9ds0;

/* Private functions ------------------------------------------------------- */
/**
 * Sensor Hub thread
 */
static THD_FUNCTION(SensorHub_Thread, arg);

/**
 * Initializes and configures sensor drivers
 * @return Returns true if operation was successful. Otherwise returns false.
 */
static bool SensorHub_Init(void);

/**
 * Handles events received by Sensor Hub thread
 * @param eventmask Received eventmask
 */
static void SensorHub_ProcessEvents(eventmask_t eventmask);

/**
 * Reads data from accelerometer
 * @param data Pointer to location where data will be stored
 * @return Returns true if operation was successful. Otherwise returns false.
 */
static bool SensorHub_ReadAccelData(vector3f_t *data);

/**
 * Reads data from gyroscope
 * @param data Pointer to location where data will be stored
 * @return Returns true if operation was successful. Otherwise returns false.
 */
static bool SensorHub_ReadGyroData(vector3f_t *data);

/**
 * Reads data from magnetometer
 * @param data Pointer to location where data will be stored
 * @return Returns true if operation was successful. Otherwise returns false.
 */
static bool SensorHub_ReadMagData(vector3f_t *data);

/**
 * Reads data from pressure sensor
 * @param data Pointer to location where data will be stored
 * @return Returns true if operation was successful. Otherwise returns false.
 */
static bool SensorHub_ReadPressData(float *data);

/* Exported functions ------------------------------------------------------ */
void SensorHub_Start(void)
{
	// Start sensor hub thread
	SensorHub_ThreadPtr = chThdCreateStatic(SensorHub_ThreadWA,
			sizeof(SensorHub_ThreadWA),
			SENSOR_HUB_PRIORITY,
			SensorHub_Thread,
			NULL);
}

thread_t* SensorHub_GetThreadPtr(void)
{
	return SensorHub_ThreadPtr;
}

/* Private functions ------------------------------------------------------- */
static THD_FUNCTION(SensorHub_Thread, arg)
{
	(void)arg;
	eventmask_t eventmask;

	chRegSetThreadName("Sensor Hub Thread");

	/* Initialize sensor drivers */
	if(SensorHub_Init()) {
		LOG("Sensor Hub: Sensors initialized.\r\n");
	} else {
		LOG("Sensor Hub: An error occurred during sensor initialization.\r\n");

		/* TODO: Better error handling */
		while(true) chThdSleep(MS2ST(1000));
	}

	while(true) {
		eventmask = chEvtWaitAny(ALL_EVENTS);
		SensorHub_ProcessEvents(eventmask);
	}

	chThdExit(0);
}

static bool SensorHub_Init(void)
{
	/* Initialize BMP280 */
	bmp280_lld_init(&bmp280);
	if(bmp280_init(&bmp280) != 0) return false;

	/* Configure BMP280 */
	if(bmp280_set_work_mode(BMP280_ULTRA_HIGH_RESOLUTION_MODE) != 0) return false;
	if(bmp280_set_standby_durn(BMP280_STANDBY_TIME_250_MS) != 0) return false;
	if(bmp280_set_power_mode(BMP280_NORMAL_MODE) != 0) return false;

	/* Initialize LSM9DS0 */
	lsm9ds0_lld_init(&lsm9ds0);
	if(lsm9ds0_Init(&lsm9ds0) != LSM9DS0_OK) return false;

	/* Configure LSM9DS0 */
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_GYRO_ODR, LSM9DS0_GYRO_ODR_95_HZ);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_GYRO_FS, LSM9DS0_GYRO_FS_245_DPS);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_GYRO_POWER_DOWN, LSM9DS0_GYRO_POWER_DOWN_DIS);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_GYRO_DRDY, LSM9DS0_GYRO_DRDY_EN);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_ACCEL_ODR, LSM9DS0_ACCEL_ODR_100_HZ);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_ACCEL_FS, LSM9DS0_ACCEL_FS_4_G);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_ACCEL_DRDY_INT1, LSM9DS0_ACCEL_DRDY_INT1_EN);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_ACCEL_DRDY_INT2, LSM9DS0_ACCEL_DRDY_INT2_DIS);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_MAG_ODR, LSM9DS0_MAG_ODR_100_HZ);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_MAG_FS, LSM9DS0_MAG_FS_12_GAUSS);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_MAG_RESOLUTION, LSM9DS0_MAG_RESOLUTION_HIGH);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_MAG_MODE, LSM9DS0_MAG_MODE_CONTINUOUS);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_MAG_DRDY_INT1, LSM9DS0_MAG_DRDY_INT1_DIS);
	lsm9ds0_SetParam(&lsm9ds0, LSM9DS0_MAG_DRDY_INT2, LSM9DS0_MAG_DRDY_INT2_EN);

	return true;
}

static void SensorHub_ProcessEvents(eventmask_t eventmask)
{
	vector3f_t vector3f;
	float scalar;

	/* Process events */
	if(eventmask & SENSOR_HUB_EVT_GYRO_NEW_DATA) {
		if(SensorHub_ReadGyroData(&vector3f)) {
			LOG("Gyro: %04.2f, %04.2f, %04.2f\r\n", vector3f.x, vector3f.y, vector3f.z);

			/* TODO: Process data */
		} else {
			/* TODO: Error handling */
		}
	}
	if(eventmask & SENSOR_HUB_EVT_ACCEL_NEW_DATA) {
		if(SensorHub_ReadAccelData(&vector3f)) {
			LOG("Accel: %02.4f, %02.4f, %02.4f\r\n", vector3f.x, vector3f.y, vector3f.z);

			/* TODO: Process data */
		} else {
			/* TODO: Error handling */
		}
	}
	if(eventmask & SENSOR_HUB_EVT_MAG_NEW_DATA) {
		if(SensorHub_ReadMagData(&vector3f)) {
			LOG("Mag: %03.3f, %03.3f, %03.3f\r\n", vector3f.x, vector3f.y, vector3f.z);

			/* TODO: Process data */
		} else {
			/* TODO: Error handling */
		}
	}
	if(eventmask & SENSOR_HUB_EVT_PRESS_NEW_DATA) {
		if(SensorHub_ReadPressData(&scalar)) {
			LOG("Press: %04.2f\r\n", scalar);

			/* TODO: Process data */
		} else {
			/* TODO: Error handling */
		}
	}
}

static bool SensorHub_ReadAccelData(vector3f_t *data)
{
	lsm9ds0_Result_t result;

	result = lsm9ds0_GetAccelData(&lsm9ds0, &data->x, &data->y, &data->z);
	if(result != LSM9DS0_OK) return false;

	return true;
}

static bool SensorHub_ReadGyroData(vector3f_t *data)
{
	lsm9ds0_Result_t result;

	result = lsm9ds0_GetGyroData(&lsm9ds0, &data->x, &data->y, &data->z);
	if(result != LSM9DS0_OK) return false;

	return true;
}

static bool SensorHub_ReadMagData(vector3f_t *data)
{
	lsm9ds0_Result_t result;

	result = lsm9ds0_GetMagData(&lsm9ds0, &data->x, &data->y, &data->z);
	if(result != LSM9DS0_OK) return false;

	return true;
}

static bool SensorHub_ReadPressData(float *data)
{
	int32_t temperature;
	uint32_t pressure;

	if(bmp280_read_pressure_temperature(&pressure, &temperature) != 0) return false;

	*data = ((float)pressure)/100;

	return true;
}
