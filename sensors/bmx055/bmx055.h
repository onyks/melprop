/*
 * bmx055.h
 *
 *  Created on: 8 gru 2015
 *      Author: jacek
 */

#ifndef BMX055_H_
#define BMX055_H_

#include <stdint.h>

typedef void (*CSfunc_ptr)(uint8_t CSstate);
typedef	void (*spiRWfunc_ptr)(uint8_t *inBuff, uint8_t *outBuff, uint32_t size);

typedef struct
{
	// Hardware functions
	CSfunc_ptr		accCS;
	CSfunc_ptr		gyrCS;
	CSfunc_ptr		magCS;
	spiRWfunc_ptr	spiReadWrite;

	// private data
	uint8_t		acc_range;
	uint8_t		gyr_range;

	// Magnetometer trim values
	int8_t		dig_x1;
	int8_t		dig_y1;
	int8_t		dig_x2;
	int8_t		dig_y2;

	uint16_t	dig_z1;
	int16_t		dig_z2;
	int16_t		dig_z3;
	int16_t		dig_z4;

	uint8_t		dig_xy1;
	int8_t		dig_xy2;

	uint16_t	dig_xyz1;
}bmx055_t;

typedef struct
{
	int16_t x;
	int16_t y;
	int16_t z;
}int16_xyz;

typedef struct
{
	float x;
	float y;
	float z;
} float_xyz;

uint8_t		bmx055_init(bmx055_t *dev, CSfunc_ptr accCS, CSfunc_ptr gyrCS, CSfunc_ptr magCS, spiRWfunc_ptr spiReadWrite);

void		bmx055_accIntPinConfig(bmx055_t *dev, uint8_t intPin, uint8_t openDrain, uint8_t activeHigh);
void		bmx055_accIntConfig(bmx055_t *dev, uint32_t interrupt, uint8_t mapping, uint8_t filtered);
void		bmx055_accIntEnable(bmx055_t *dev, uint32_t interrupt, uint8_t enable);

void		bmx055_accRangeSet(bmx055_t *dev, uint8_t range);
void		bmx055_accBandwidthSet(bmx055_t *dev, uint8_t bandwidth);

void		bmx055_accReadXYZ(bmx055_t *dev, int16_xyz *out);
void		bmx055_accReadXYZ_f(bmx055_t *dev, float_xyz *out);
float		bmx055_accReadTemp_f(bmx055_t *dev);

void		bmx055_gyrReadXYZ(bmx055_t *dev, int16_xyz *out);
void		bmx055_gyrRangeSet(bmx055_t *dev, uint8_t range);
void		bmx055_gyrBandwidthSet(bmx055_t *dev, uint8_t bandwidth);

void		bmx055_magReadXYZ(bmx055_t *dev, int16_xyz *out);
void		bmx055_magDataRateSet(bmx055_t *dev, uint8_t rate);

/*
 * Interrupt definitions
 */
#define BMX055_ACC_INT_FLAT				0x80
#define BMX055_ACC_INT_ORIENT			0x40
#define BMX055_ACC_INT_S_TAP			0x20
#define BMX055_ACC_INT_D_TAP			0x10
#define BMX055_ACC_INT_RESERVED1		0x08
#define BMX055_ACC_INT_SLOPE_Z			0x04
#define BMX055_ACC_INT_SLOPE_Y			0x02
#define BMX055_ACC_INT_SLOPE_X			0x01

#define BMX055_ACC_INT_RESERVED2		0x8000
#define BMX055_ACC_INT_FWM				0x4000
#define BMX055_ACC_INT_FFULL			0x2000
#define BMX055_ACC_INT_DATA				0x1000
#define BMX055_ACC_INT_LOW				0x0800
#define BMX055_ACC_INT_HIGH_Z			0x0400
#define BMX055_ACC_INT_HIGH_Y			0x0200
#define BMX055_ACC_INT_HIGH_X			0x0100

#define BMX055_ACC_INT_RESERVED3		0x800000
#define BMX055_ACC_INT_RESERVED4		0x400000
#define BMX055_ACC_INT_RESERVED5		0x200000
#define BMX055_ACC_INT_RESERVED6		0x100000
#define BMX055_ACC_INT_SLO_NO_MOT_SEL	0x080000
#define BMX055_ACC_INT_SLO_NO_MOT_Z		0x040000
#define BMX055_ACC_INT_SLO_NO_MOT_Y		0x020000
#define BMX055_ACC_INT_SLO_NO_MOT_X		0x010000

#define BMX055_ACC_INT_SLOPE			0x07
#define BMX055_ACC_INT_HIGH				0x0700
#define BMX055_ACC_INT_SLO_NO_MOT		0x0F0000

#define BMX055_ACC_INT1					0x01
#define BMX055_ACC_INT2					0x02

/*
 * Range definitions
 */
#define BMX055_ACC_RANGE_2G		0x03
#define BMX055_ACC_RANGE_4G		0x05
#define BMX055_ACC_RANGE_8G		0x08
#define BMX055_ACC_RANGE_16G	0x0C

#define BMX055_GYR_RANGE_2000	0x00
#define BMX055_GYR_RANGE_1000	0x01
#define BMX055_GYR_RANGE_500	0x02
#define BMX055_GYR_RANGE_250	0x03
#define BMX055_GYR_RANGE_125	0x04

/*
 * Bandwidth definitions
 */
#define BMX055_ACC_BW_1000HZ	0x0f
#define BMX055_ACC_BW_500HZ		0x0e
#define BMX055_ACC_BW_250HZ		0x0d
#define BMX055_ACC_BW_125HZ		0x0c
#define BMX055_ACC_BW_62_5HZ	0x0b
#define BMX055_ACC_BW_31_25HZ	0x0a
#define BMX055_ACC_BW_15_63HZ	0x09
#define BMX055_ACC_BW_7_81HZ	0x08

/*
 * Filter bandwidth (data rate)
 */
#define BMX055_GYR_BW_32HZ		0x07 // (100Hz)
#define BMX055_GYR_BW_64HZ		0x06 // (200Hz)
#define BMX055_GYR_BW_12HZ		0x05 // (100Hz)
#define BMX055_GYR_BW_23HZ		0x04 // (200Hz)
#define BMX055_GYR_BW_47HZ		0x03 // (400Hz)
#define BMX055_GYR_BW_116HZ		0x02 // (1000Hz)
#define BMX055_GYR_BW_230HZ		0x01 // (2000Hz)
#define BMX055_GYR_BW_523HZ		0x00 // (2000Hz)

/*
 * Magnetometer Data Rate
 */
#define BMX055_MAG_RATE_10HZ	0x00
#define BMX055_MAG_RATE_2HZ		0x01
#define BMX055_MAG_RATE_6HZ		0x02
#define BMX055_MAG_RATE_8HZ		0x03
#define BMX055_MAG_RATE_15HZ	0x04
#define BMX055_MAG_RATE_20HZ	0x05
#define BMX055_MAG_RATE_25HZ	0x06
#define BMX055_MAG_RATE_30HZ	0x07

/*
 * Errors definitions
 */
#define BMX055_ERROR_OK			0x00
#define BMX055_ERROR_GENERAL	0x01
#define BMX055_ERROR_ACC		0x02
#define BMX055_ERROR_GYR		0x04
#define BMX055_ERROR_MAG		0x08

/*
 * Chip IDs
 */
#define BMX055_ACC_CHIPID		0xFA
#define BMX055_GYR_CHIPID		0x0F
#define BMX055_MAG_CHIPID		0x32

/*  Registers descrition  */
#define	BMX055_REG_READ			0x80
#define	BMX055_REG_WRITE		0x00

/*
 * Accelerometer registers
 */
#define	BMX055_ACC_REG_BGW_CHIPID		0x00 // RO

#define	BMX055_ACC_REG_RESERVED1		0x01 // Reserved

#define	BMX055_ACC_REG_ACCD_X_LSB		0x02 // RO
#define	BMX055_ACC_REG_ACCD_X_MSB		0x03 // RO
#define	BMX055_ACC_REG_ACCD_Y_LSB		0x04 // RO
#define	BMX055_ACC_REG_ACCD_Y_MSB		0x05 // RO
#define	BMX055_ACC_REG_ACCD_Z_LSB		0x06 // RO
#define	BMX055_ACC_REG_ACCD_Z_MSB		0x07 // RO
#define	BMX055_ACC_REG_ACCD_TEMP		0x08 // RO
#define	BMX055_ACC_REG_INT_STATUS_0		0x09 // RO
#define	BMX055_ACC_REG_INT_STATUS_1		0x0A // RO
#define	BMX055_ACC_REG_INT_STATUS_2		0x0B // RO
#define	BMX055_ACC_REG_INT_STATUS_3		0x0C // RO

#define	BMX055_ACC_REG_RESERVED2		0x0D // Reserved

#define	BMX055_ACC_REG_FIFO_STATUS		0x0E // RO
#define	BMX055_ACC_REG_PMU_RANGE		0x0F // RW
#define	BMX055_ACC_REG_PMU_BW			0x10 // RW
#define	BMX055_ACC_REG_PMU_LPW			0x11 // RW
#define	BMX055_ACC_REG_PMU_LOW_POWER	0x12 // RW
#define	BMX055_ACC_REG_ACCD_HBW			0x13 // RW
#define	BMX055_ACC_REG_BGW_SOFTRESET	0x14 // WO

#define	BMX055_ACC_REG_RESERVED3		0x15 // Reserved

#define	BMX055_ACC_REG_INT_EN_0			0x16 // RW
#define	BMX055_ACC_REG_INT_EN_1			0x17 // RW
#define	BMX055_ACC_REG_INT_EN_2			0x18 // RW
#define	BMX055_ACC_REG_INT_MAP_0		0x19 // RW
#define	BMX055_ACC_REG_INT_MAP_1		0x1A // RW
#define	BMX055_ACC_REG_INT_MAP_2		0x1B // RW

#define	BMX055_ACC_REG_RESERVED4		0x1C // Reserved
#define	BMX055_ACC_REG_RESERVED5		0x1D // Reserved

#define	BMX055_ACC_REG_INT_SRC			0x1E // RW
#define	BMX055_ACC_REG_RESERVED6		0x1F // Reserved
#define	BMX055_ACC_REG_INT_OUT_CTRL		0x20 // RW
#define	BMX055_ACC_REG_INT_RST_LATCH	0x21 // RW

#define	BMX055_ACC_REG_INT_0			0x22 // RW
#define	BMX055_ACC_REG_INT_1			0x23 // RW
#define	BMX055_ACC_REG_INT_2			0x24 // RW
#define	BMX055_ACC_REG_INT_3			0x25 // RW
#define	BMX055_ACC_REG_INT_4			0x26 // RW
#define	BMX055_ACC_REG_INT_5			0x27 // RW
#define	BMX055_ACC_REG_INT_6			0x28 // RW
#define	BMX055_ACC_REG_INT_7			0x29 // RW
#define	BMX055_ACC_REG_INT_8			0x2A // RW
#define	BMX055_ACC_REG_INT_9			0x2B // RW
#define	BMX055_ACC_REG_INT_A			0x2C // RW
#define	BMX055_ACC_REG_INT_B			0x2D // RW
#define	BMX055_ACC_REG_INT_C			0x2E // RW
#define	BMX055_ACC_REG_INT_D			0x2F // RW

#define	BMX055_ACC_REG_FIFO_CONFIG_0	0x30 // RW
#define	BMX055_ACC_REG_RESERVED7		0x31 // Reserved

#define	BMX055_ACC_REG_PMU_SELF_TEST	0x32 // RW

#define	BMX055_ACC_REG_TRIM_NVM_CTRL	0x33 // RW
#define	BMX055_ACC_REG_BGW_SPI3_WDT		0x34 // RW
#define	BMX055_ACC_REG_RESERVED8		0x35 // Reserved

#define	BMX055_ACC_REG_OFC_CTRL			0x36 // RW
#define	BMX055_ACC_REG_OFC_SETTING		0x37 // RW
#define	BMX055_ACC_REG_OFC_OFFSET_X		0x38 // RW
#define	BMX055_ACC_REG_OFC_OFFSET_Y		0x39 // RW
#define	BMX055_ACC_REG_OFC_OFFSET_Z		0x3A // RW

#define	BMX055_ACC_REG_TRIM_GP0			0x3B // RW
#define	BMX055_ACC_REG_TRIM_GP1			0x3C // RW
#define	BMX055_ACC_REG_RESERVED9		0x3D // Reserved

#define	BMX055_ACC_REG_FIFO_CONFIG_1	0x3E // RW
#define	BMX055_ACC_REG_FIFO_DATA		0x3F // R
/*
 * END of Accelerometer registers
 */

/*
 * Gyroscope registers
 */
#define BMX055_GYR_REG_CHIP_ID			0x00 // RO
#define BMX055_GYR_REG_RESERVED1		0x01 // Reserved

/* Output data */
#define BMX055_GYR_REG_RATE_X_LSB		0x02 // RO
#define BMX055_GYR_REG_RATE_X_MSB		0x03 // RO
#define BMX055_GYR_REG_RATE_Y_LSB		0x04 // RO
#define BMX055_GYR_REG_RATE_Y_MSB		0x05 // RO
#define BMX055_GYR_REG_RATE_Z_LSB		0x06 // RO
#define BMX055_GYR_REG_RATE_Z_MSB		0x07 // RO

#define BMX055_GYR_REG_RESERVED2		0x08 // Reserved

/* Interrupt status */
#define BMX055_GYR_REG_INT_STATUS_0		0x09 // RO
#define BMX055_GYR_REG_INT_STATUS_1		0x0A // RO
#define BMX055_GYR_REG_INT_STATUS_2		0x0B // RO
#define BMX055_GYR_REG_INT_STATUS_3		0x0C // RO

#define BMX055_GYR_REG_RESERVED3		0x0D // Reserved

#define BMX055_GYR_REG_FIFO_STATUS		0x0E // RO
#define BMX055_GYR_REG_RANGE			0x0F // RW
#define BMX055_GYR_REG_BW				0x10 // RW
#define BMX055_GYR_REG_LPM1				0x11 // RW
#define BMX055_GYR_REG_LPM2				0x12 // RW
#define BMX055_GYR_REG_RATE_HBW			0x13 // RW
#define BMX055_GYR_REG_BGW_SOFTRESET	0x14 // WO

#define BMX055_GYR_REG_INT_EN_0			0x15 // RW
#define BMX055_GYR_REG_INT_EN_1			0x16 // RW

#define BMX055_GYR_REG_INT_MAP_0		0x17 // RW
#define BMX055_GYR_REG_INT_MAP_1		0x18 // RW
#define BMX055_GYR_REG_INT_MAP_2		0x19 // RW

#define BMX055_GYR_REG_1A				0x1A // RW
#define BMX055_GYR_REG_1B				0x1B // RW
#define BMX055_GYR_REG_1C				0x1C // RW

#define BMX055_GYR_REG_RESERVED4		0x1D // Reserved

#define BMX055_GYR_REG_1E				0x1E // RW

#define BMX055_GYR_REG_RESERVED5		0x1F // Reserved
#define BMX055_GYR_REG_RESERVED6		0x20 // Reserved

#define BMX055_GYR_REG_INT_RST_LATCH	0x21 // WO/RW

#define BMX055_GYR_REG_HIGH_TH_X		0x22 // RW
#define BMX055_GYR_REG_HIGH_DUR_X		0x23 // RW
#define BMX055_GYR_REG_HIGH_TH_Y		0x24 // RW
#define BMX055_GYR_REG_HIGH_DUR_Y		0x25 // RW
#define BMX055_GYR_REG_HIGH_TH_Z		0x26 // RW
#define BMX055_GYR_REG_HIGH_DUR_Z		0x27 // RW

#define BMX055_GYR_REG_RESERVED7		0x28 // Reserved
#define BMX055_GYR_REG_RESERVED8		0x29 // Reserved
#define BMX055_GYR_REG_RESERVED9		0x2A // Reserved
#define BMX055_GYR_REG_RESERVED10		0x2B // Reserved
#define BMX055_GYR_REG_RESERVED11		0x2C // Reserved
#define BMX055_GYR_REG_RESERVED12		0x2D // Reserved
#define BMX055_GYR_REG_RESERVED13		0x2E // Reserved
#define BMX055_GYR_REG_RESERVED14		0x2F // Reserved
#define BMX055_GYR_REG_RESERVED15		0x30 // Reserved

#define BMX055_GYR_REG_SOC				0x31 // RW
#define BMX055_GYR_REG_A_FOC			0x32 // RW/WO
#define BMX055_GYR_REG_TRIM_NVM_CTRL	0x33 // RO/WO/RW
#define BMX055_GYR_REG_BGW_SPI3_WDT		0x34 // RW

#define BMX055_GYR_REG_RESERVED16		0x35 // Reserved

#define BMX055_GYR_REG_OFC1				0x36 // RW
#define BMX055_GYR_REG_OFC2				0x37 // RW
#define BMX055_GYR_REG_OFC3				0x38 // RW
#define BMX055_GYR_REG_OFC4				0x39 // RW
#define BMX055_GYR_REG_TRIM_GP0			0x3A // RW
#define BMX055_GYR_REG_TRIM_GP1			0x3B // RW
#define BMX055_GYR_REG_BIST				0x3C // RO/WO
#define BMX055_GYR_REG_FIFO_CONFIG_0	0x3D // RW
#define BMX055_GYR_REG_FIFO_CONFIG_1	0x3E // RW
#define BMX055_GYR_REG_FIFO_DATA		0x3F // RO
/*
 * END of Gyroscope registers
 */


/*
 * Magnetometer registers
 */
#define	BMX055_MAG_REG_CHIPID			0x40 // RO
#define	BMX055_MAG_REG_RESERVED1		0x41 // Reserved

#define	BMX055_MAG_REG_DATA_X_LSB		0x42 // RO
#define	BMX055_MAG_REG_DATA_X_MSB		0x43 // RO
#define	BMX055_MAG_REG_DATA_Y_LSB		0x44 // RO
#define	BMX055_MAG_REG_DATA_Y_MSB		0x45 // RO
#define	BMX055_MAG_REG_DATA_Z_LSB		0x46 // RO
#define	BMX055_MAG_REG_DATA_Z_MSB		0x47 // RO
#define	BMX055_MAG_REG_DATA_R_LSB		0x48 // RO
#define	BMX055_MAG_REG_DATA_R_MSB		0x49 // RO

#define	BMX055_MAG_REG_INT_STAT			0x4A // RO
#define	BMX055_MAG_REG_POWER_CTRL		0x4B // RW in suspend mode
#define	BMX055_MAG_REG_CONTROL			0x4C // RW
#define	BMX055_MAG_REG_INT_CTRL			0x4D // RW
#define	BMX055_MAG_REG_INT_AXIS_CTRL	0x4E // RW

#define	BMX055_MAG_REG_LOW_THRESHOLD	0x4F // RW
#define	BMX055_MAG_REG_HIGH_THRESHOLD	0x50 // RW

#define	BMX055_MAG_REG_REP_XY			0x51 // RW
#define	BMX055_MAG_REG_REP_Z			0x52 // RW

/*
 * Magnetometer Trim Extended Registers
 */
#define BMX055_MAG_REG_DIG_X1                      (0x5D)
#define BMX055_MAG_REG_DIG_Y1                      (0x5E)
#define BMX055_MAG_REG_DIG_Z4_LSB                  (0x62)
#define BMX055_MAG_REG_DIG_Z4_MSB                  (0x63)
#define BMX055_MAG_REG_DIG_X2                      (0x64)
#define BMX055_MAG_REG_DIG_Y2                      (0x65)
#define BMX055_MAG_REG_DIG_Z2_LSB                  (0x68)
#define BMX055_MAG_REG_DIG_Z2_MSB                  (0x69)
#define BMX055_MAG_REG_DIG_Z1_LSB                  (0x6A)
#define BMX055_MAG_REG_DIG_Z1_MSB                  (0x6B)
#define BMX055_MAG_REG_DIG_XYZ1_LSB                (0x6C)
#define BMX055_MAG_REG_DIG_XYZ1_MSB                (0x6D)
#define BMX055_MAG_REG_DIG_Z3_LSB                  (0x6E)
#define BMX055_MAG_REG_DIG_Z3_MSB                  (0x6F)
#define BMX055_MAG_REG_DIG_XY2                     (0x70)
#define BMX055_MAG_REG_DIG_XY1                     (0x71)
/*
 * END of Magnetometer registers
 */

#endif /* BMX055_H_ */
