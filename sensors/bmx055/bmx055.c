/*
 * bmx055.c
 *
 *  Created on: 8 gru 2015
 *      Author: jacek
 */

#include "bmx055.h"
#include <stddef.h>

static inline uint8_t isACCptrOK(bmx055_t *dev);
static inline uint8_t isGYRptrOK(bmx055_t *dev);
static inline uint8_t isMAGptrOK(bmx055_t *dev);

static void bmx055_magReadTrim(bmx055_t *dev);

static inline void setRegister(bmx055_t *dev, CSfunc_ptr cs, uint8_t addr, uint8_t value)
{
	uint8_t toSend[2] = { addr, value };

	cs(0);
	dev->spiReadWrite(NULL, toSend, 2);
	cs(1);
}

static inline uint8_t readRegister(bmx055_t *dev, CSfunc_ptr cs, uint8_t addr)
{
	uint8_t toSend[2] = { addr | BMX055_REG_READ, 0 };
	uint8_t recv[2] = {0};

	cs(0);
	dev->spiReadWrite(recv, toSend, 2);
	cs(1);

	return recv[1];
}

static void ACC_setRegister(bmx055_t *dev, uint8_t addr, uint8_t value)
{
	uint8_t toSend[2] = { addr, value };

	dev->accCS(0);
	dev->spiReadWrite(NULL, toSend, 2);
	dev->accCS(1);
}

static uint8_t ACC_readRegister(bmx055_t *dev, uint8_t addr)
{
	uint8_t toSend[2] = { addr | BMX055_REG_READ, 0 };
	uint8_t recv[2] = {0};

	dev->accCS(0);
	dev->spiReadWrite(recv, toSend, 2);
	dev->accCS(1);

	return recv[1];
}

static void GYR_setRegister(bmx055_t *dev, uint8_t addr, uint8_t value)
{
	setRegister(dev, dev->gyrCS, addr, value);
}

static uint8_t GYR_readRegister(bmx055_t *dev, uint8_t addr)
{
	return readRegister(dev, dev->gyrCS, addr);
}

static void MAG_setRegister(bmx055_t *dev, uint8_t addr, uint8_t value)
{
	setRegister(dev, dev->magCS, addr, value);
}

static uint8_t MAG_readRegister(bmx055_t *dev, uint8_t addr)
{
	return readRegister(dev, dev->magCS, addr);
}

uint8_t	bmx055_init(bmx055_t *dev, CSfunc_ptr accCS, CSfunc_ptr gyrCS, CSfunc_ptr magCS, spiRWfunc_ptr spiReadWrite)
{
	uint8_t retErr = BMX055_ERROR_OK;

	if(dev == NULL)
		return BMX055_ERROR_GENERAL;
	if(spiReadWrite == NULL)
		return BMX055_ERROR_GENERAL;

	dev->spiReadWrite = spiReadWrite;

	// Early magnetometer soft reset
	dev->magCS = magCS;
	if(magCS != NULL)
	{
		MAG_setRegister(dev, BMX055_MAG_REG_POWER_CTRL, 0x83); // Enable sleep mode
	}

	// Accelerometer init
	dev->accCS = accCS;
	if(accCS == NULL)
		retErr |= BMX055_ERROR_ACC;
	else
	{
		if( ACC_readRegister(dev, BMX055_ACC_REG_BGW_CHIPID) != BMX055_ACC_CHIPID ) // if CHIP_ID not valid
			retErr |= BMX055_ERROR_ACC; // return Accelerometer error
		else
		{ // Accelerometer init
			dev->acc_range = BMX055_ACC_RANGE_2G;

		} // End of Acc init
	}


	// Gyroscope init
	dev->gyrCS = gyrCS;
	if(gyrCS == NULL)
		retErr |= BMX055_ERROR_GYR;
	else
	{
		if( GYR_readRegister(dev, BMX055_GYR_REG_CHIP_ID) != BMX055_GYR_CHIPID ) // if CHIP_ID not valid
			retErr |= BMX055_ERROR_GYR; // return Gyroscope error
		else
		{ // Gyroscope init
			dev->gyr_range = BMX055_GYR_RANGE_2000;
		} // End of Gyroscope init
	}


	// Magnetometer init
	dev->magCS = magCS;
	if(magCS == NULL)
		retErr |= BMX055_ERROR_MAG;
	else
	{
//		MAG_setRegister(dev, BMX055_MAG_REG_POWER_CTRL, 0x01); // Enable sleep mode
		if( MAG_readRegister(dev, BMX055_MAG_REG_CHIPID) != BMX055_MAG_CHIPID ) // if CHIP_ID not valid
			retErr |= BMX055_ERROR_MAG; // return Magnetometer error
		else
		{ // Magnetometer init
			bmx055_magReadTrim(dev);
		} // End of Magnetometer init
	}

	return retErr;
}

/******************************************************
 *
 * 				Accelerometer Functions
 *
 ******************************************************/

//uint8_t bmx055_accIntIsSignaled(bmx055_t *dev, uint32_t interrupt)
//{
//
//}

/*
 * \brief Function sets physical interrupt pins of device.
 *
 * \param intPin		- interrupt pin of device
 * \param openDrain		- boolean value, is openDrain=1 or push-pull=0
 * \param activeHigh	- 1 if interrupt pin should be active-high or 0 if active-low
 *
 * If *dev is invalid, do nothing.
 */
void bmx055_accIntPinConfig(bmx055_t *dev, uint8_t intPin, uint8_t openDrain, uint8_t activeHigh)
{
	if( !isACCptrOK(dev) ) // check HW functions are present
		return;

	uint8_t regData = ACC_readRegister(dev, BMX055_ACC_REG_INT_OUT_CTRL);

	uint8_t set = (activeHigh>0) | ((openDrain>0) << 1); // combine settings in 2bit value

	if(intPin & BMX055_ACC_INT1)
		regData = (regData & 0xFC) | set; // set new reg values for INT1 pin

	if(intPin & BMX055_ACC_INT2)
		regData = (regData & 0xF3) | (set << 2);// set new reg values for INT2 pin

	ACC_setRegister(dev, BMX055_ACC_REG_INT_OUT_CTRL, regData);
}

/*
 * INT_S_TAP and INT_D_TAP has common filtering settings.
 * BMX055_ACC_INT_SLO_NO_MOT_Z, BMX055_ACC_INT_SLO_NO_MOT_Y and BMX055_ACC_INT_SLO_NO_MOT_X has common mapping settings.
 * BMX055_ACC_INT_SLOPE_Z, BMX055_ACC_INT_SLOPE_Y and BMX055_ACC_INT_SLOPE_X has common mapping settings.
 * BMX055_ACC_INT_HIGH_Z, BMX055_ACC_INT_HIGH_Y and BMX055_ACC_INT_HIGH_X has common mapping settings.
 */
void bmx055_accIntConfig(bmx055_t *dev, uint32_t interrupt, uint8_t mapping, uint8_t filtered)
{
	if( !isACCptrOK(dev) ) // check HW functions are present
		return;

	/*
	 * HW INT pins mapping
	 */
	if( (interrupt & (	BMX055_ACC_INT_FLAT |
						BMX055_ACC_INT_ORIENT |
						BMX055_ACC_INT_S_TAP |
						BMX055_ACC_INT_D_TAP |
						BMX055_ACC_INT_SLO_NO_MOT |
						BMX055_ACC_INT_SLOPE |
						BMX055_ACC_INT_LOW	|
						BMX055_ACC_INT_HIGH
						) ) ) // check if there is BMX055_ACC_REG_INT_MAP_0 or BMX055_ACC_REG_INT_MAP_2 change
	{
		uint8_t mask_slo_no_mot	= (interrupt & BMX055_ACC_INT_SLO_NO_MOT)	? 0x08 : 0x00;
		uint8_t mask_slope		= (interrupt & BMX055_ACC_INT_SLOPE)		? 0x04 : 0x00;
		uint8_t mask_high		= (interrupt & BMX055_ACC_INT_HIGH)			? 0x02 : 0x00;
		uint8_t mask_low		= (interrupt & BMX055_ACC_INT_LOW)			? 0x01 : 0x00;
		uint8_t mask = (interrupt & 0xF0) | mask_slo_no_mot | mask_slope | mask_high | mask_low;

		uint8_t enable_int1 = (mapping & BMX055_ACC_INT1) ? 0xFF : 0x00;
		uint8_t enable_int2 = (mapping & BMX055_ACC_INT2) ? 0xFF : 0x00;

		{ // BMX055_ACC_REG_INT_MAP_0 change
			uint8_t oldReg = ACC_readRegister(dev, BMX055_ACC_REG_INT_MAP_0);
			uint8_t new = (oldReg & ~mask) | (enable_int1 & mask);
			ACC_setRegister(dev, BMX055_ACC_REG_INT_MAP_0, new);
		}

		{ // BMX055_ACC_REG_INT_MAP_2 change
			uint8_t oldReg = ACC_readRegister(dev, BMX055_ACC_REG_INT_MAP_2);
			uint8_t new = (oldReg & ~mask) | (enable_int2 & mask);
			ACC_setRegister(dev, BMX055_ACC_REG_INT_MAP_2, new);
		}
	}

	if( (interrupt & (	BMX055_ACC_INT_DATA |
						BMX055_ACC_INT_FWM |
						BMX055_ACC_INT_FFULL
						) ) ) // check if there is BMX055_ACC_REG_INT_MAP_1 change
	{
		uint8_t oldReg = ACC_readRegister(dev, BMX055_ACC_REG_INT_MAP_1);
		uint8_t enable_int = ((mapping & BMX055_ACC_INT1) ? 0x0F : 0x00) | ((mapping & BMX055_ACC_INT2) ? 0xF0 : 0x00);

		uint8_t mask_data	= (interrupt & BMX055_ACC_INT_DATA)		? 0x80 : 0x00;
		uint8_t mask_fwm	= (interrupt & BMX055_ACC_INT_FWM)		? 0x40 : 0x00;
		uint8_t mask_ffull	= (interrupt & BMX055_ACC_INT_FFULL)	? 0x20 : 0x00;
		uint8_t mask = (interrupt & (BMX055_ACC_INT_FWM | BMX055_ACC_INT_FFULL | BMX055_ACC_INT_DATA)) >> 12; // first 4 bytes
		mask |= (mask_data | mask_fwm | mask_ffull);

		uint8_t new = (oldReg & ~mask) | (enable_int & mask);
		ACC_setRegister(dev, BMX055_ACC_REG_INT_MAP_1, new);
	}

	/*
	 * Filtering settings
	 */
	if( (interrupt & (	BMX055_ACC_INT_DATA |
						BMX055_ACC_INT_S_TAP |
						BMX055_ACC_INT_D_TAP |
						BMX055_ACC_INT_SLO_NO_MOT |
						BMX055_ACC_INT_SLOPE |
						BMX055_ACC_INT_LOW	|
						BMX055_ACC_INT_HIGH
						) ) ) // check if there is BMX055_ACC_REG_INT_MAP_1 change
	{
		uint8_t oldReg = ACC_readRegister(dev, BMX055_ACC_REG_INT_SRC);
		uint8_t enable = (!filtered) ? 0xFF : 0x00;

		uint8_t mask_data		= (interrupt & BMX055_ACC_INT_DATA)	>> 7; // 0x20
		uint8_t mask_tap		= (interrupt & (BMX055_ACC_INT_S_TAP | BMX055_ACC_INT_D_TAP))	? 0x10 : 0x00;
		uint8_t mask_slo_no_mot	= (interrupt & BMX055_ACC_INT_SLO_NO_MOT)						? 0x08 : 0x00;
		uint8_t mask_slope		= (interrupt & BMX055_ACC_INT_SLOPE)							? 0x04 : 0x00;
		uint8_t mask_high		= (interrupt & BMX055_ACC_INT_HIGH)								? 0x02 : 0x00;
		uint8_t mask_low		= (interrupt & BMX055_ACC_INT_LOW)								? 0x01 : 0x00;
		uint8_t mask = mask_data | mask_tap | mask_slo_no_mot | mask_slope | mask_high | mask_low;

		uint8_t new = (oldReg & ~mask) | (enable & mask);
		ACC_setRegister(dev, BMX055_ACC_REG_INT_SRC, new);
	}
}

void bmx055_accIntEnable(bmx055_t *dev, uint32_t interrupt, uint8_t enable)
{
	if( !isACCptrOK(dev) ) // check HW functions are present
		return;

	uint8_t enable_v = (enable) ? 0xFF : 0x00; // set all ones if enable = 1 else all zeros

	if(interrupt & 0xFF) // if there is INT_EN_0 reg data change
	{
		uint8_t oldReg = ACC_readRegister(dev, BMX055_ACC_REG_INT_EN_0);
		uint8_t mask = interrupt & 0xFF;

		uint8_t new = (oldReg & ~mask) | (enable_v & mask);
		ACC_setRegister(dev, BMX055_ACC_REG_INT_EN_0, new);
	}

	if(interrupt & 0xFF00) // if there is INT_EN_1 reg data change
	{
		uint8_t oldReg = ACC_readRegister(dev, BMX055_ACC_REG_INT_EN_1);
		uint8_t mask = (interrupt >> 8) & 0xFF;

		uint8_t new = (oldReg & ~mask) | (enable_v & mask);
		ACC_setRegister(dev, BMX055_ACC_REG_INT_EN_1, new);
	}

	if(interrupt & 0xFF0000) // if there is INT_EN_2 reg data change
	{
		uint8_t oldReg = ACC_readRegister(dev, BMX055_ACC_REG_INT_EN_2);
		uint8_t mask = (interrupt >> 16) & 0xFF;

		uint8_t new = (oldReg & ~mask) | (enable_v & mask);
		ACC_setRegister(dev, BMX055_ACC_REG_INT_EN_2, new);
	}
}

void bmx055_accRangeSet(bmx055_t *dev, uint8_t range)
{
	if(dev == NULL)
		return;
	if(dev->accCS == NULL)
		return;
	if(dev->spiReadWrite == NULL)
		return;

	if(range == 0)
		return;

	ACC_setRegister(dev, BMX055_ACC_REG_PMU_RANGE, range & 0x0f);
	dev->acc_range = range & 0x0f;
}

void bmx055_accBandwidthSet(bmx055_t *dev, uint8_t bandwidth)
{
	if( !isACCptrOK(dev) )
		return;

	if(bandwidth > BMX055_ACC_BW_1000HZ)
		bandwidth = BMX055_ACC_BW_1000HZ;

	if(bandwidth < BMX055_ACC_BW_7_81HZ)
		bandwidth = BMX055_ACC_BW_7_81HZ;

	uint8_t reg_data = bandwidth & 0x1F;

	ACC_setRegister(dev, BMX055_ACC_REG_PMU_BW, reg_data);
}

void bmx055_accReadXYZ(bmx055_t *dev, int16_xyz *out)
{
	if(dev == NULL)
		return;
	if(dev->accCS == NULL)
		return;
	if(dev->spiReadWrite == NULL)
		return;

	if(out == NULL)
		return;

	uint32_t rwSize = 7;
	uint8_t dataSend[7] = { BMX055_ACC_REG_ACCD_X_LSB | BMX055_REG_READ, 0 };
	uint8_t dataRecv[7] = { 0 };

	dev->accCS(0);
	dev->spiReadWrite(dataRecv, dataSend, rwSize);
	dev->accCS(1);

	uint16_t x = ((dataRecv[1] & 0xF0)) | (dataRecv[2] << 8);
	uint16_t y = ((dataRecv[3] & 0xF0)) | (dataRecv[4] << 8);
	uint16_t z = ((dataRecv[5] & 0xF0)) | (dataRecv[6] << 8);

	switch(dev->acc_range)
	{
	default:
	case BMX055_ACC_RANGE_2G:
		out->x = (int16_t)x >> 4;
		out->y = (int16_t)y >> 4;
		out->z = (int16_t)z >> 4;
		break;

	case BMX055_ACC_RANGE_4G:
		out->x = (int16_t)x >> 3;
		out->y = (int16_t)y >> 3;
		out->z = (int16_t)z >> 3;
		break;

	case BMX055_ACC_RANGE_8G:
		out->x = (int16_t)x >> 2;
		out->y = (int16_t)y >> 2;
		out->z = (int16_t)z >> 2;
		break;

	case BMX055_ACC_RANGE_16G:
		out->x = (int16_t)x >> 1;
		out->y = (int16_t)y >> 1;
		out->z = (int16_t)z >> 1;
		break;
	}
}

void bmx055_accReadXYZ_f(bmx055_t *dev, float_xyz *out)
{
	if(dev == NULL)
		return;
	if(dev->accCS == NULL)
		return;
	if(dev->spiReadWrite == NULL)
		return;

	if(out == NULL)
		return;

	uint32_t rwSize = 7;
	uint8_t dataSend[7] = { BMX055_ACC_REG_ACCD_X_LSB | BMX055_REG_READ, 0 };
	uint8_t dataRecv[7] = { 0 };

	dev->accCS(0);
	dev->spiReadWrite(dataRecv, dataSend, rwSize);
	dev->accCS(1);

	uint16_t x = ((dataRecv[1] & 0xF0)) | (dataRecv[2] << 8);
	uint16_t y = ((dataRecv[3] & 0xF0)) | (dataRecv[4] << 8);
	uint16_t z = ((dataRecv[5] & 0xF0)) | (dataRecv[6] << 8);

	int16_t ix, iy, iz;

	switch(dev->acc_range)
	{
	default:
	case BMX055_ACC_RANGE_2G:
		ix = (int16_t)x >> 4;
		iy = (int16_t)y >> 4;
		iz = (int16_t)z >> 4;
		break;

	case BMX055_ACC_RANGE_4G:
		ix = (int16_t)x >> 3;
		iy = (int16_t)y >> 3;
		iz = (int16_t)z >> 3;
		break;

	case BMX055_ACC_RANGE_8G:
		ix = (int16_t)x >> 2;
		iy = (int16_t)y >> 2;
		iz = (int16_t)z >> 2;
		break;

	case BMX055_ACC_RANGE_16G:
		ix = (int16_t)x >> 1;
		iy = (int16_t)y >> 1;
		iz = (int16_t)z >> 1;
		break;
	}

	out->x = ((float)ix) / 1024.0f;
	out->y = ((float)iy) / 1024.0f;
	out->z = ((float)iz) / 1024.0f;
}

float bmx055_accReadTemp_f(bmx055_t *dev)
{
	if( !isACCptrOK(dev) )
		return 0;

	uint8_t temp_reg = ACC_readRegister(dev, BMX055_ACC_REG_ACCD_TEMP);

	float temp = ( ((int8_t)temp_reg) + 23.0);
	return temp;
}

/******************************************************
 *
 * 				Gyroscope Functions
 *
 ******************************************************/

void bmx055_gyrReadXYZ(bmx055_t *dev, int16_xyz *out)
{
	if( !isGYRptrOK(dev) )
		return;

	if(out == NULL)
		return;

	uint32_t rwSize = 7;
	uint8_t dataSend[7] = { BMX055_GYR_REG_RATE_X_LSB | BMX055_REG_READ, 0 };
	uint8_t dataRecv[7] = { 0 };

	dev->gyrCS(0);
	dev->spiReadWrite(dataRecv, dataSend, rwSize);
	dev->gyrCS(1);

	uint16_t x = ((dataRecv[1])) | (dataRecv[2] << 8);
	uint16_t y = ((dataRecv[3])) | (dataRecv[4] << 8);
	uint16_t z = ((dataRecv[5])) | (dataRecv[6] << 8);

	out->x = (int16_t)x;
	out->y = (int16_t)y;
	out->z = (int16_t)z;
}

void bmx055_gyrRangeSet(bmx055_t *dev, uint8_t range)
{
	if( !isGYRptrOK(dev) )
		return;

	if(range > BMX055_GYR_RANGE_125)
		range = BMX055_GYR_RANGE_125;

	GYR_setRegister(dev, BMX055_GYR_REG_RANGE, range & 0x07);
	dev->gyr_range = range & 0x07;
}

/*
 * \brief Set filter bandwidth and data rate
 *
 * \param bandwidth - corresponds with desired data rate.
 */
void bmx055_gyrBandwidthSet(bmx055_t *dev, uint8_t bandwidth)
{
	if( !isGYRptrOK(dev) )
		return;

	if(bandwidth > BMX055_GYR_BW_32HZ)
		bandwidth = BMX055_GYR_BW_32HZ;

	GYR_setRegister(dev, BMX055_GYR_REG_BW, bandwidth & 0x0F);
}


/******************************************************
 *
 * 				Magnetometer Functions
 *
 ******************************************************/

#define BMM050_FLIP_OVERFLOW_ADCVAL		-4096
#define BMM050_OVERFLOW_OUTPUT			-32768
#define BMM050_HALL_OVERFLOW_ADCVAL		-16384

int16_t bmm050_compensate_X(bmx055_t *dev, int16_t mag_data_x, uint16_t data_r)
{
	if(dev == NULL)
		return 0;

	int16_t inter_retval = 0;
	/* no overflow */
	if (mag_data_x != BMM050_FLIP_OVERFLOW_ADCVAL)
	{
		if ((data_r != 0) && (dev->dig_xyz1 != 0))
		{
			inter_retval = ((int16_t)(((uint16_t)((((int32_t)dev->dig_xyz1) << 14) / (data_r != 0 ? data_r : dev->dig_xyz1))) - ((uint16_t)0x4000)));
		} else {
			inter_retval = BMM050_OVERFLOW_OUTPUT;
			return inter_retval;
		}
		inter_retval = ((int16_t)((((int32_t)mag_data_x) * ((((((((int32_t)dev->dig_xy2) * ((((int32_t)inter_retval) * ((int32_t)inter_retval)) >> 7)) + (((int32_t)inter_retval) * ((int32_t)(((int16_t)dev->dig_xy1)	<< 7)))) >> 9) + ((int32_t)0x100000)) * ((int32_t)(((int16_t)dev->dig_x2) + ((int16_t)0xA0)))) >> 12)) >> 13)) + (((int16_t)dev->dig_x1) << 3);
	} else {
		/* overflow */
		inter_retval = BMM050_OVERFLOW_OUTPUT;
	}
	return inter_retval;
}

int16_t bmm050_compensate_Y(bmx055_t *dev, int16_t mag_data_y, uint16_t data_r)
{
	if(dev == NULL)
			return 0;

	int16_t inter_retval = 0;
	 /* no overflow */
	if (mag_data_y != BMM050_FLIP_OVERFLOW_ADCVAL)
	{
		if ((data_r != 0) && (dev->dig_xyz1 != 0))
		{
			inter_retval = ((int16_t)(((uint16_t)(((	(int32_t)dev->dig_xyz1)<< 14)/(data_r != 0 ? data_r : dev->dig_xyz1))) - ((uint16_t)0x4000)));
		} else {
			inter_retval = BMM050_OVERFLOW_OUTPUT;
			return inter_retval;
		}
		inter_retval = ((int16_t)((((int32_t)mag_data_y) * ((((((((int32_t)	dev->dig_xy2) * ((((int32_t) inter_retval) *((int32_t)inter_retval)) >>	7))	+ (((int32_t)inter_retval) *((int32_t)(((int16_t)dev->dig_xy1) <<7))))>> 9) +((int32_t)0x100000)) * ((int32_t)(((int16_t)dev->dig_y2) + ((int16_t)0xA0)))) >> 12))	>> 13)) + (((int16_t)dev->dig_y1) << 3);
	} else {
		/* overflow */
		inter_retval = BMM050_OVERFLOW_OUTPUT;
	}
	return inter_retval;
}

#define BMM050_NEGATIVE_SATURATION_Z            (-32767)
#define BMM050_POSITIVE_SATURATION_Z            (32767)

int16_t bmm050_compensate_Z(bmx055_t *dev, int16_t mag_data_z, uint16_t data_r)
{
	if(dev == NULL)
			return 0;

	int32_t retval = 0;

	if ((mag_data_z != BMM050_HALL_OVERFLOW_ADCVAL) ) /* no overflow */
	{
		if ((dev->dig_z2 != 0)	&& (dev->dig_z1 != 0) && (data_r != 0)	&& (dev->dig_xyz1 != 0))
		{
			retval = (((((int32_t)(mag_data_z - dev->dig_z4))<< 15) -((((int32_t)dev->dig_z3) * ((int32_t)(((int16_t)data_r) -((int16_t) dev->dig_xyz1))))>> 2))/(dev->dig_z2 + ((int16_t)(((((int32_t)dev->dig_z1) * ((((int16_t)data_r)<< 1)))+(1 << 15))>> 16))));
		} else {
			retval = BMM050_OVERFLOW_OUTPUT;
			return retval;
		}
		/* saturate result to +/- 2 microTesla */
		if (retval > BMM050_POSITIVE_SATURATION_Z)
		{
			retval =  BMM050_POSITIVE_SATURATION_Z;
		} else {
			if (retval < BMM050_NEGATIVE_SATURATION_Z)
				retval = BMM050_NEGATIVE_SATURATION_Z;
		}
	} else {
		/* overflow */
		retval = BMM050_OVERFLOW_OUTPUT;
	}
	return (int16_t)retval;
}

static void bmx055_magReadTrim(bmx055_t *dev)
{
	if( !isMAGptrOK(dev) ) // check HW functions are present
		return;

	dev->dig_x1 = MAG_readRegister(dev, BMX055_MAG_REG_DIG_X1);
	dev->dig_y1 = MAG_readRegister(dev, BMX055_MAG_REG_DIG_Y1);
	dev->dig_x2 = MAG_readRegister(dev, BMX055_MAG_REG_DIG_X2);
	dev->dig_y2 = MAG_readRegister(dev, BMX055_MAG_REG_DIG_Y2);

	dev->dig_xy1 = MAG_readRegister(dev, BMX055_MAG_REG_DIG_XY1);
	dev->dig_xy2 = MAG_readRegister(dev, BMX055_MAG_REG_DIG_XY2);

	dev->dig_z1 = (MAG_readRegister(dev, BMX055_MAG_REG_DIG_Z1_MSB) << 8) | MAG_readRegister(dev, BMX055_MAG_REG_DIG_Z1_LSB);
	dev->dig_z2 = (MAG_readRegister(dev, BMX055_MAG_REG_DIG_Z2_MSB) << 8) | MAG_readRegister(dev, BMX055_MAG_REG_DIG_Z2_LSB);
	dev->dig_z3 = (MAG_readRegister(dev, BMX055_MAG_REG_DIG_Z3_MSB) << 8) | MAG_readRegister(dev, BMX055_MAG_REG_DIG_Z3_LSB);
	dev->dig_z4 = (MAG_readRegister(dev, BMX055_MAG_REG_DIG_Z4_MSB) << 8) | MAG_readRegister(dev, BMX055_MAG_REG_DIG_Z4_LSB);

	dev->dig_xyz1= (MAG_readRegister(dev, BMX055_MAG_REG_DIG_XYZ1_MSB) & 0x7F << 8) | MAG_readRegister(dev, BMX055_MAG_REG_DIG_XYZ1_LSB);
}

void bmx055_magReadXYZ(bmx055_t *dev, int16_xyz *out)
{
	if( !isMAGptrOK(dev) ) // check HW functions are present
		return;

	if(out == NULL)
		return;

	MAG_setRegister(dev, 0x4C, 0x02); // set forced mode

	uint32_t rwSize = 9;
	uint8_t dataSend[9] = { BMX055_MAG_REG_DATA_X_LSB | BMX055_REG_READ, 0 };
	uint8_t dataRecv[9] = { 0 };

	dev->magCS(0);
	dev->spiReadWrite(dataRecv, dataSend, rwSize);
	dev->magCS(1);

	uint16_t x = ((dataRecv[1] & 0xF8)) | (dataRecv[2] << 8);
	uint16_t y = ((dataRecv[3] & 0xF8)) | (dataRecv[4] << 8);
	uint16_t z = ((dataRecv[5] & 0xFE)) | (dataRecv[6] << 8);
	uint16_t r = ((dataRecv[7] & 0xFC)) | (dataRecv[8] << 8);

	out->x = (int16_t)x >> 3;
	out->x = bmm050_compensate_X(dev, out->x, r >> 2);

	out->y = (int16_t)y >> 3;
	out->y = bmm050_compensate_Y(dev, out->y, r >> 2);

	out->z = (int16_t)z >> 1;
	out->z = bmm050_compensate_Z(dev, out->z, r >> 2);

//	out->z = (int16_t)r >> 2;
}

void bmx055_magDataRateSet(bmx055_t *dev, uint8_t rate)
{
	if( !isMAGptrOK(dev) ) // check HW functions are present
		return;

	uint8_t regData = MAG_readRegister(dev, BMX055_MAG_REG_CONTROL);

	regData = (regData & 0xC7) | ( (rate & 0x07) << 3);

	MAG_setRegister(dev, BMX055_MAG_REG_CONTROL, regData);
}

/*
 * Other functions
 */

static inline uint8_t isACCptrOK(bmx055_t *dev)
{
	if(dev == NULL)
		return 0;
	if(dev->accCS == NULL)
		return 0;
	if(dev->spiReadWrite == NULL)
		return 0;

	return 1;
}

static inline uint8_t isGYRptrOK(bmx055_t *dev)
{
	if(dev == NULL)
		return 0;
	if(dev->gyrCS == NULL)
		return 0;
	if(dev->spiReadWrite == NULL)
		return 0;

	return 1;
}

static inline uint8_t isMAGptrOK(bmx055_t *dev)
{
	if(dev == NULL)
		return 0;
	if(dev->magCS == NULL)
		return 0;
	if(dev->spiReadWrite == NULL)
		return 0;

	return 1;
}

