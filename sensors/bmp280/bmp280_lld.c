/*
 * bmp280_lld.c
 *
 *  Created on: 29.10.2016
 *      Author: krystian
 */

#include <string.h>
#include "ch.h"
#include "hal.h"
#include "spi_lib.h"
#include "bsp.h"
#include "bmp280.h"
#include "bmp280_lld.h"

/* Private definitions ----------------------------------------------------- */
#define	SPI_READ	0x80
#define SPI_WRITE	0x7F

/* Exported functions ------------------------------------------------------ */
void bmp280_lld_init(struct bmp280_t *device)
{
	// TODO: Make sure spi_lib was initialized
	// ...

	/* Add pointer to the low-level functions that will be used by
	 * the high-level driver. */
	device->bus_read = bmp280_lld_spi_read;
	device->bus_write = bmp280_lld_spi_write;
	device->delay_msec = bmp280_lld_delay_ms;
}

s8 bmp280_lld_spi_read(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 length)
{
	(void)dev_addr;
	u8 txBuffer[length + 1], rxBuffer[length + 1], i;

	if(reg_data == NULL) return BMP280_ERROR;

	/* Fill TX buffer with zeros */
	memset(txBuffer, 0, length + 1);

	/*	For the SPI mode only 7 bits of register addresses are used.
	The MSB of register address denotes the type of SPI data transfer, whether
	read/write (read as 1/write as 0). */
	txBuffer[0] = reg_addr | SPI_READ;

	// Exchange data
	bmp280_chip_select(true);
	spi_readWrite(rxBuffer, txBuffer, length + 1);
	bmp280_chip_select(false);

	/* The first read data is discarded, since extra write operation has to be
	 * performed in order to transmit register address. */
	for(i = 0; i < length; i++) {
		reg_data[i] = rxBuffer[i + 1];
	}

	return BMP280_SUCCESS;
}

s8 bmp280_lld_spi_write(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 length)
{
	(void)dev_addr;
	u8 buffer[length * 2], i, index;

	if(reg_data == NULL) return BMP280_ERROR;

	/* Fill-in buffer. Sensor requires us to prepend each byte of the data
	 * with address of the destination register. */
	for (i = 0; i < length; i++) {
		/* the operation of (reg_addr++)&0x7F done as per the
		SPI communication protocol specified in the data sheet*/
		index = i * 2;
		buffer[index] = (reg_addr++) & SPI_WRITE;
		buffer[index + 1] = *(reg_data + i);
	}

	// Exchange data
	bmp280_chip_select(true);
	spi_readWrite(NULL, buffer, length + 1);
	bmp280_chip_select(false);

	return BMP280_SUCCESS;
}

void  bmp280_lld_delay_ms(u32 ms)
{
	chThdSleep(MS2ST(ms));
}
