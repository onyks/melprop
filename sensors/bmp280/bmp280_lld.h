/*
 * bmp280_lld.h
 *
 *  Created on: 29.10.2016
 *      Author: krystian
 */

#ifndef BMP280_LLD_H_
#define BMP280_LLD_H_

/* Exported functions ------------------------------------------------------ */
/**
 * Initialized sensor's low-level driver
 * @param device Pointer to the sensor's instance
 */
void bmp280_lld_init(struct bmp280_t *device);

/**
 * Read data from the sensor using SPI interface
 * @param dev_addr Unused param (left here for compatibility with high level driver)
 * @param reg_addr Address of the first register to read data from
 * @param reg_data Pointer to the buffer where read data will be stored
 * @param length Number of consecutive registers to read
 * @return If transaction was successful, a zero value is returned. Otherwise -1 is returned.
 */
s8 bmp280_lld_spi_read(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 length);

/**
 * Write data to the sensor using SPI interface
 * @param dev_addr Unused param (left here for compatibility with high level driver)
 * @param reg_addr Address of the first register to write data to
 * @param reg_data Pointer to the buffer containing data to write
 * @param length Number of bytes (registers) to write
 * @return If transaction was successful, a zero value is returned. Otherwise -1 is returned.
 */
s8 bmp280_lld_spi_write(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 length);

/**
 * Delay routine
 * @param ms Number of miliseconds
 */
void  bmp280_lld_delay_ms(u32 ms);

#endif /* BMP280_LLD_H_ */
