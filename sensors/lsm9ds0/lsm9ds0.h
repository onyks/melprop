/*
 * lsm9ds0.h
 *
 *  Created on: 30.10.2016
 *      Author: krystian
 */

#ifndef LSM9DS0_H_
#define LSM9DS0_H_

#include <inttypes.h>
#include <stdbool.h>

/* --------------------------------------------------------------------------
 * - Register map
 * -------------------------------------------------------------------------- */
/* Gyroscope */
#define LSM9DS0_WHO_AM_I_G						((uint8_t)0x0F)
#define LSM9DS0_CTRL_REG1_G						((uint8_t)0x20)
#define LSM9DS0_CTRL_REG2_G						((uint8_t)0x21)
#define LSM9DS0_CTRL_REG3_G						((uint8_t)0x22)
#define LSM9DS0_CTRL_REG4_G						((uint8_t)0x23)
#define LSM9DS0_CTRL_REG5_G						((uint8_t)0x24)
#define LSM9DS0_REFERENCE_G						((uint8_t)0x25)
#define LSM9DS0_STATUS_REG_G					((uint8_t)0x27)
#define LSM9DS0_OUT_X_L_G						((uint8_t)0x28)
#define LSM9DS0_OUT_X_H_G						((uint8_t)0x29)
#define LSM9DS0_OUT_Y_L_G						((uint8_t)0x2A)
#define LSM9DS0_OUT_Y_H_G						((uint8_t)0x2B)
#define LSM9DS0_OUT_Z_L_G						((uint8_t)0x2C)
#define LSM9DS0_OUT_Z_H_G						((uint8_t)0x2D)
#define LSM9DS0_FIFO_CTRL_REG_G					((uint8_t)0x2E)
#define LSM9DS0_FIFO_SRC_REG_G					((uint8_t)0x2F)
#define LSM9DS0_INT1_CFG_G						((uint8_t)0x30)
#define LSM9DS0_INT1_SRC_G						((uint8_t)0x31)
#define LSM9DS0_INT1_TSH_XH_G					((uint8_t)0x32)
#define LSM9DS0_INT1_TSH_XL_G					((uint8_t)0x33)
#define LSM9DS0_INT1_TSH_YH_G					((uint8_t)0x34)
#define LSM9DS0_INT1_TSH_YL_G					((uint8_t)0x35)
#define LSM9DS0_INT1_TSH_ZH_G					((uint8_t)0x36)
#define LSM9DS0_INT1_TSH_ZL_G					((uint8_t)0x37)
#define LSM9DS0_INT1_DURATION_G					((uint8_t)0x38)

/* Accelerometer and magnetometer */
#define LSM9DS0_OUT_TEMP_L_XM					((uint8_t)0x05)
#define LSM9DS0_OUT_TEMP_H_XM					((uint8_t)0x06)
#define LSM9DS0_STATUS_REG_M					((uint8_t)0x07)
#define LSM9DS0_OUT_X_L_M						((uint8_t)0x08)
#define LSM9DS0_OUT_X_H_M						((uint8_t)0x09)
#define LSM9DS0_OUT_Y_L_M						((uint8_t)0x0A)
#define LSM9DS0_OUT_Y_H_M						((uint8_t)0x0B)
#define LSM9DS0_OUT_Z_L_M						((uint8_t)0x0C)
#define LSM9DS0_OUT_Z_H_M						((uint8_t)0x0D)
#define LSM9DS0_WHO_AM_I_XM						((uint8_t)0x0F)
#define LSM9DS0_INT_CTRL_REG_M					((uint8_t)0x12)
#define LSM9DS0_INT_SRC_REG_M					((uint8_t)0x13)
#define LSM9DS0_INT_THS_L_M						((uint8_t)0x14)
#define LSM9DS0_INT_THS_H_M						((uint8_t)0x15)
#define LSM9DS0_OFFSET_X_L_M					((uint8_t)0x16)
#define LSM9DS0_OFFSET_X_H_M					((uint8_t)0x17)
#define LSM9DS0_OFFSET_Y_L_M					((uint8_t)0x18)
#define LSM9DS0_OFFSET_Y_H_M					((uint8_t)0x19)
#define LSM9DS0_OFFSET_Z_L_M					((uint8_t)0x1A)
#define LSM9DS0_OFFSET_Z_H_M					((uint8_t)0x1B)
#define LSM9DS0_REFERENCE_X						((uint8_t)0x1C)
#define LSM9DS0_REFERENCE_Y						((uint8_t)0x1D)
#define LSM9DS0_REFERENCE_Z						((uint8_t)0x1E)
#define LSM9DS0_CTRL_REG0_XM					((uint8_t)0x1F)
#define LSM9DS0_CTRL_REG1_XM					((uint8_t)0x20)
#define LSM9DS0_CTRL_REG2_XM					((uint8_t)0x21)
#define LSM9DS0_CTRL_REG3_XM					((uint8_t)0x22)
#define LSM9DS0_CTRL_REG4_XM					((uint8_t)0x23)
#define LSM9DS0_CTRL_REG5_XM					((uint8_t)0x24)
#define LSM9DS0_CTRL_REG6_XM					((uint8_t)0x25)
#define LSM9DS0_CTRL_REG7_XM					((uint8_t)0x26)
#define LSM9DS0_STATUS_REG_A					((uint8_t)0x27)
#define LSM9DS0_OUT_X_L_A						((uint8_t)0x28)
#define LSM9DS0_OUT_X_H_A						((uint8_t)0x29)
#define LSM9DS0_OUT_Y_L_A						((uint8_t)0x2A)
#define LSM9DS0_OUT_Y_H_A						((uint8_t)0x2B)
#define LSM9DS0_OUT_Z_L_A						((uint8_t)0x2C)
#define LSM9DS0_OUT_Z_H_A						((uint8_t)0x2D)
#define LSM9DS0_FIFO_CTRL_REG					((uint8_t)0x2E)
#define LSM9DS0_FIFO_SRC_REG					((uint8_t)0x2F)
#define LSM9DS0_INT_GEN_1_REG					((uint8_t)0x30)
#define LSM9DS0_INT_GEN_1_SRC					((uint8_t)0x31)
#define LSM9DS0_INT_GEN_1_THS					((uint8_t)0x32)
#define LSM9DS0_INT_GEN_1_DURATION				((uint8_t)0x33)
#define LSM9DS0_INT_GEN_2_REG					((uint8_t)0x34)
#define LSM9DS0_INT_GEN_2_SRC					((uint8_t)0x35)
#define LSM9DS0_INT_GEN_2_THS					((uint8_t)0x36)
#define LSM9DS0_INT_GEN_2_DURATION				((uint8_t)0x37)
#define LSM9DS0_CLICK_CFG						((uint8_t)0x38)
#define LSM9DS0_CLICK_SRC						((uint8_t)0x39)
#define LSM9DS0_CLICK_THS						((uint8_t)0x3A)
#define LSM9DS0_TIME_LIMIT						((uint8_t)0x3B)
#define LSM9DS0_TIME_LATENCY					((uint8_t)0x3C)
#define LSM9DS0_TIME_WINDOW						((uint8_t)0x3D)
#define LSM9DS0_ACT_THS							((uint8_t)0x3E)
#define LSM9DS0_ACT_DUR							((uint8_t)0x3F)

/* --------------------------------------------------------------------------
 * - SENSOR PARAMS
 * -------------------------------------------------------------------------- */
/**
 * Sensor parameters
 */
typedef enum {
	LSM9DS0_GYRO_ODR,       //!< Gyroscope output data rate
	LSM9DS0_GYRO_FS,        //!< Gyroscope full-scale
	LSM9DS0_GYRO_POWER_DOWN,//!< Gyroscope power-down
	LSM9DS0_GYRO_DRDY,		//!< Gyroscope data ready notification on DRDY_G pin
	LSM9DS0_ACCEL_ODR,      //!< Accelerometer output data rate
	LSM9DS0_ACCEL_FS,       //!< Accelerometer full-scale
	LSM9DS0_ACCEL_DRDY_INT1,//!< Accelerometer data ready notification on INT1_XM pin
	LSM9DS0_ACCEL_DRDY_INT2,//!< Accelerometer data ready notification on INT1_XM pin
	LSM9DS0_MAG_ODR,        //!< Magnetometer output data rate
	LSM9DS0_MAG_FS,         //!< Magnetometer full-scale
	LSM9DS0_MAG_RESOLUTION,	//!< Magnetometer resolution
	LSM9DS0_MAG_MODE,		//!< Magnetometer mode
	LSM9DS0_MAG_DRDY_INT1,	//!< Magnetometer data ready notification on INT1_XM pin
	LSM9DS0_MAG_DRDY_INT2,	//!< Magnetometer data ready notification on INT2_XM pin
	_LSM9DS0_PARAM_END      //!< Not a real param. Marks the end of the enum.
} lsm9ds0_Param_t;

/* Gyroscope output data rate */
#define LSM9DS0_GYRO_ODR_95_HZ				(0b00)
#define LSM9DS0_GYRO_ODR_190_HZ				(0b01)
#define LSM9DS0_GYRO_ODR_380_HZ				(0b10)
#define LSM9DS0_GYRO_ODR_760_HZ				(0b11)

/* Gyroscope full-scale */
#define LSM9DS0_GYRO_FS_245_DPS				(0b00)
#define LSM9DS0_GYRO_FS_500_DPS				(0b01)
#define LSM9DS0_GYRO_FS_2000_DPS			(0b11)

/* Gyroscope power-down mode */
#define LSM9DS0_GYRO_POWER_DOWN_EN			(0b0) /* Enable */
#define LSM9DS0_GYRO_POWER_DOWN_DIS			(0b1) /* Disable */

/* Gyroscope data ready notification on DRDY_G pin */
#define LSM9DS0_GYRO_DRDY_EN				(0b1) /* Enable */
#define LSM9DS0_GYRO_DRDY_DIS				(0b0) /* Disable */

/* Accelerometer output data rate */
#define LSM9DS0_ACCEL_ODR_POWER_DOWN		(0b0000)
#define LSM9DS0_ACCEL_ODR_3_125_HZ			(0b0001)
#define LSM9DS0_ACCEL_ODR_6_25_HZ			(0b0010)
#define LSM9DS0_ACCEL_ODR_12_5_HZ			(0b0011)
#define LSM9DS0_ACCEL_ODR_25_HZ				(0b0100)
#define LSM9DS0_ACCEL_ODR_50_HZ				(0b0101)
#define LSM9DS0_ACCEL_ODR_100_HZ			(0b0110)
#define LSM9DS0_ACCEL_ODR_200_HZ			(0b0111)
#define LSM9DS0_ACCEL_ODR_400_HZ			(0b1000)
#define LSM9DS0_ACCEL_ODR_800_HZ			(0b1001)
#define LSM9DS0_ACCEL_ODR_1600_HZ			(0b1010)

/* Accelerometer full-scale */
#define LSM9DS0_ACCEL_FS_2_G				(0b000)
#define LSM9DS0_ACCEL_FS_4_G				(0b001)
#define LSM9DS0_ACCEL_FS_6_G				(0b010)
#define LSM9DS0_ACCEL_FS_8_G				(0b011)
#define LSM9DS0_ACCEL_FS_16_G				(0b100)

/* Accelerometer data ready notification on INT1_XM pin */
#define LSM9DS0_ACCEL_DRDY_INT1_EN			(0b1) /* Enable */
#define LSM9DS0_ACCEL_DRDY_INT1_DIS			(0b0) /* Disable */

/* Accelerometer data ready notification on INT1_XM pin */
#define LSM9DS0_ACCEL_DRDY_INT2_EN			(0b1) /* Enable */
#define LSM9DS0_ACCEL_DRDY_INT2_DIS			(0b0) /* Disable */

/* Magnetometer output data rate */
#define LSM9DS0_MAG_ODR_3_125_HZ			(0b000)
#define LSM9DS0_MAG_ODR_6_25_HZ				(0b001)
#define LSM9DS0_MAG_ODR_12_5_HZ				(0b010)
#define LSM9DS0_MAG_ODR_25_HZ				(0b011)
#define LSM9DS0_MAG_ODR_50_HZ				(0b100)
#define LSM9DS0_MAG_ODR_100_HZ				(0b101)

/* Magnetometer full-scale */
#define LSM9DS0_MAG_FS_2_GAUSS				(0b00)
#define LSM9DS0_MAG_FS_4_GAUSS				(0b01)
#define LSM9DS0_MAG_FS_8_GAUSS				(0b10)
#define LSM9DS0_MAG_FS_12_GAUSS				(0b11)

/* Magnetometer resolution */
#define LSM9DS0_MAG_RESOLUTION_LOW			(0b00)
#define LSM9DS0_MAG_RESOLUTION_HIGH			(0b11)

/* Magnetometer mode */
#define LSM9DS0_MAG_MODE_SINGLE				(0b01)
#define LSM9DS0_MAG_MODE_CONTINUOUS			(0b00)
#define LSM9DS0_MAG_MODE_POWER_DOWN			(0b10)

/* Magnetometer data ready notification on INT1_XM pin */
#define LSM9DS0_MAG_DRDY_INT1_EN			(0b1) /* Enable */
#define LSM9DS0_MAG_DRDY_INT1_DIS			(0b0) /* Disable */

/* Magnetometer data ready notification on INT2_XM pin */
#define LSM9DS0_MAG_DRDY_INT2_EN			(0b1) /* Enable */
#define LSM9DS0_MAG_DRDY_INT2_DIS			(0b0) /* Disable */

/* --------------------------------------------------------------------------
 * - ERROR CODES
 * -------------------------------------------------------------------------- */
typedef int8_t lsm9ds0_Result_t;

#define LSM9DS0_OK					((lsm9ds0_Result_t)0)

/** Unspecified error */
#define LSM9DS0_ERR					((lsm9ds0_Result_t)-1)

/** Bus (I2C or SPI) error */
#define LSM9DS0_BUS_ERR				((lsm9ds0_Result_t)-2)

/** Wrong value of the WHO_AM_I register */
#define LSM9DS0_WRONG_WHO_AM_I_ERR	((lsm9ds0_Result_t)-3)

/** Null pointer */
#define LSM9DS0_NULL_PTR_ERR		((lsm9ds0_Result_t)-4)

/** Unknown sensor param */
#define LSM9DS0_UNKNOWN_PARAM		((lsm9ds0_Result_t)-5)

/* --------------------------------------------------------------------------
 * - MISCELLANEOUS
 * -------------------------------------------------------------------------- */
/* WHO_AM_I values */
#define LSM9DS0_WHO_AM_I_G_VALUE				((uint8_t)0b11010100)
#define LSM9DS0_WHO_AM_I_XM_VALUE				((uint8_t)0b01001001)

/* SPI read/write function pointers ----------------------------------------- */
#define LSM9DS0_WRITE_FUNC_PTR	bool (*bus_write)(uint8_t, uint8_t, uint8_t *, \
		uint8_t, bool)

#define LSM9DS0_READ_FUNC_PTR	bool (*bus_read)(uint8_t, uint8_t, uint8_t *, \
		uint8_t, bool)

/* --------------------------------------------------------------------------
 * - SENSOR INSTANCE
 * -------------------------------------------------------------------------- */
struct lsm9ds0 {
	/* When using SPI interface, IDs are used by low-level driver (provided by
	 * the user) to choose the right chip select pin.
	 * When using I2C interface, IDs can store device address or can be used to
	 * resolve device address in the low-level driver (provided by the user).
	 * IDs have to be used even when using only single LSM9DS0 sensor, since
	 * gyroscope uses separate interface from accelerometer and magnetic sensor.
	 * IDs are not used in any way by THIS driver. */
	/**
	 * ID of the gyroscope
	 */
	uint8_t g_id;

	/**
	 * ID of the accelerometer and magnetic sensor
	 */
	uint8_t xm_id;

	/**
	 * Current gyroscope sensitivity
	 */
	uint32_t gyro_sensitivity;

	/**
	 * Current accelerometer sensitivity
	 */
	uint32_t accel_sensitivity;

	/**
	 * Current magnetometer sensitivity
	 */
	uint32_t mag_sensitivity;

	/**
	 * Pointer to the low-level write function provided by user
	 * @param Sensor ID (see description above for details)
	 * @param Address of the first register to write to
	 * @param Pointer to the buffer storing data to write
	 * @param Number of bytes to write
	 * @param If set to true and reading more than one byte,
	 * value of the reg_addr will be incremented after each byte
	 * @return Result of the operation
	 * @retval true Operation was successful
	 * @retval false Operation failed
	 */
	LSM9DS0_WRITE_FUNC_PTR;

	/**
	 * Pointer to the low-level read function provided by user
	 * @param Sensor ID (see description above for details)
	 * @param Address of the first register to read from
	 * @param Pointer to the buffer where read data will be stored
	 * @param Number of bytes to read
	 * @param If set to true and reading more than one byte,
	 * value of the reg_addr will be incremented after each byte
	 * @return Result of the operation
	 * @retval true Operation was successful
	 * @retval false Operation failed
	 */
	LSM9DS0_READ_FUNC_PTR;
};

typedef struct lsm9ds0 lsm9ds0_t;

/* --------------------------------------------------------------------------
 * - BASIC API
 * -------------------------------------------------------------------------- */
/**
 * Initializes LSM9DS0 sensor instance
 * @param dev[in] Pointer to the sensor instance
 * @return Returns 0 if initialization was successful.
 * Otherwise returns error code.
 */
lsm9ds0_Result_t lsm9ds0_Init(lsm9ds0_t *dev);

/**
 * Reads value of the WHO_AM_I_G register
 * @param dev[in] Pointer to the sensor instance
 * @param val[out] Pointer to the location where value of the WHO_AM_I_G register will be stored
 * @return Returns 0 if operation was successful.
 * Otherwise returns error code.
 */
lsm9ds0_Result_t lsm9ds0_GetWhoAmIG(lsm9ds0_t *dev, uint8_t *val);

/**
 * Reads value of the WHO_AM_I_XM register
 * @param dev[in] Pointer to the sensor instance
 * @param val[out] Pointer to the location where value of the WHO_AM_I_XM register will be stored
 * @return Returns 0 if operation was successful.
 * Otherwise returns error code.
 */
lsm9ds0_Result_t lsm9ds0_GetWhoAmIXM(lsm9ds0_t *dev, uint8_t *val);

/**
 * Get current parameter value
 * @param dev[in] Pointer to the sensor instance
 * @param param[in] Parameter to get
 * @param val[out] Pointer to where current value of the parameter will be stored
 * @return Returns 0 if operation was successful.
 * Otherwise returns error code.
 */
lsm9ds0_Result_t lsm9ds0_GetParam(lsm9ds0_t *dev, lsm9ds0_Param_t param, uint8_t *val);

/**
 * Set new parameter value
 * @param dev[in] Pointer to the sensor instance
 * @param param[in] Parameter to set
 * @param val[in] New parameter value
 * @return Returns 0 if operation was successful.
 * Otherwise returns error code.
 */
lsm9ds0_Result_t lsm9ds0_SetParam(lsm9ds0_t *dev, lsm9ds0_Param_t param, uint8_t val);

/* --------------------------------------------------------------------------
 * - GYROSCOPE API
 * -------------------------------------------------------------------------- */
lsm9ds0_Result_t lsm9ds0_GetGyroDataRaw(lsm9ds0_t *dev, int16_t *x, int16_t *y, int16_t *z);

lsm9ds0_Result_t lsm9ds0_GetGyroData(lsm9ds0_t *dev, float *x, float *y, float *z);

/* --------------------------------------------------------------------------
 * - ACCELEROMETER API
 * -------------------------------------------------------------------------- */
lsm9ds0_Result_t lsm9ds0_GetAccelDataRaw(lsm9ds0_t *dev, int16_t *x, int16_t *y, int16_t *z);

lsm9ds0_Result_t lsm9ds0_GetAccelData(lsm9ds0_t *dev, float *x, float *y, float *z);

/* --------------------------------------------------------------------------
 * - MAGNETOMETER API
 * -------------------------------------------------------------------------- */
lsm9ds0_Result_t lsm9ds0_GetMagDataRaw(lsm9ds0_t *dev, int16_t *x, int16_t *y, int16_t *z);

lsm9ds0_Result_t lsm9ds0_GetMagData(lsm9ds0_t *dev, float *x, float *y, float *z);

#endif /* LSM9DS0_H_ */
