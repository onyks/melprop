/*
 * lsm9ds0.c
 *
 *  Created on: 30.10.2016
 *      Author: krystian
 */

#include "lsm9ds0.h"
#include "shell_lib.h"
#include <stddef.h>

/* Interfaces --------------------------------------------------------------- */
typedef enum {LSM9DS0_INTERFACE_G, LSM9DS0_INTERFACE_XM} lsm9ds0_Interface_t;

/* Sensor param definitions ------------------------------------------------- */
typedef struct {
	uint8_t mask;
	uint8_t pos;
	uint8_t reg;
	lsm9ds0_Interface_t interface;
} lsm9ds0_ParamDef_t;

lsm9ds0_ParamDef_t param_def[] = {
	[LSM9DS0_GYRO_ODR]			= {0b11000000, 6, LSM9DS0_CTRL_REG1_G, LSM9DS0_INTERFACE_G},
	[LSM9DS0_GYRO_FS]			= {0b00110000, 4, LSM9DS0_CTRL_REG4_G, LSM9DS0_INTERFACE_G},
	[LSM9DS0_GYRO_POWER_DOWN]	= {0b00001000, 3, LSM9DS0_CTRL_REG1_G, LSM9DS0_INTERFACE_G},
	[LSM9DS0_GYRO_DRDY]			= {0b00001000, 3, LSM9DS0_CTRL_REG3_G, LSM9DS0_INTERFACE_G},
	[LSM9DS0_ACCEL_ODR]			= {0b11110000, 4, LSM9DS0_CTRL_REG1_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_ACCEL_FS]			= {0b00111000, 3, LSM9DS0_CTRL_REG2_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_ACCEL_DRDY_INT1]	= {0b00000100, 2, LSM9DS0_CTRL_REG3_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_ACCEL_DRDY_INT2]	= {0b00001000, 3, LSM9DS0_CTRL_REG4_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_MAG_ODR]			= {0b00011100, 2, LSM9DS0_CTRL_REG5_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_MAG_FS]			= {0b01100000, 5, LSM9DS0_CTRL_REG6_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_MAG_RESOLUTION]	= {0b01100000, 5, LSM9DS0_CTRL_REG5_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_MAG_MODE]			= {0b00000011, 0, LSM9DS0_CTRL_REG7_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_MAG_DRDY_INT1]		= {0b00000010, 1, LSM9DS0_CTRL_REG3_XM, LSM9DS0_INTERFACE_XM},
	[LSM9DS0_MAG_DRDY_INT2]		= {0b00000100, 2, LSM9DS0_CTRL_REG4_XM, LSM9DS0_INTERFACE_XM},
};

/* Sensitivity -------------------------------------------------------------- */
/* Gyroscope sensitivity */
#define LSM9DS0_GYRO_FS_245_DPS_SENS		((uint32_t)8750)	/* udps/LSB */
#define LSM9DS0_GYRO_FS_500_DPS_SENS		((uint32_t)17500)	/* udps/LSB */
#define LSM9DS0_GYRO_FS_2000_DPS_SENS		((uint32_t)70000)	/* udps/LSB */

/* Accelerometer sensitivity */
#define LSM9DS0_ACCEL_FS_2_G_SENS			((uint32_t)61)		/* ug/LSB */
#define LSM9DS0_ACCEL_FS_4_G_SENS			((uint32_t)122)		/* ug/LSB */
#define LSM9DS0_ACCEL_FS_6_G_SENS			((uint32_t)183)		/* ug/LSB */
#define LSM9DS0_ACCEL_FS_8_G_SENS			((uint32_t)244)		/* ug/LSB */
#define LSM9DS0_ACCEL_FS_16_G_SENS			((uint32_t)732)		/* ug/LSB */

/* Magnetometer sensitivity */
#define LSM9DS0_MAG_FS_2_GAUSS_SENS			((uint32_t)80)		/* ugauss/LSB */
#define LSM9DS0_MAG_FS_4_GAUSS_SENS			((uint32_t)160)		/* ugauss/LSB */
#define LSM9DS0_MAG_FS_8_GAUSS_SENS			((uint32_t)320)		/* ugauss/LSB */
#define LSM9DS0_MAG_FS_12_GAUSS_SENS		((uint32_t)480)		/* ugauss/LSB */

/* Private functions -------------------------------------------------------- */
static lsm9ds0_Result_t lsm9ds0_GetRegBits(lsm9ds0_t *dev, uint8_t interface,
		uint8_t reg, uint8_t mask, uint8_t pos, uint8_t *val);

static lsm9ds0_Result_t lsm9ds0_UpdateRegBits(lsm9ds0_t *dev, uint8_t interface,
		uint8_t reg, uint8_t mask, uint8_t pos, uint8_t val);

static void lsm9ds0_UpdateGyroSensitivity(lsm9ds0_t *dev, uint8_t full_scale);

static void lsm9ds0_UpdateAccelSensitivity(lsm9ds0_t *dev, uint8_t full_scale);

static void lsm9ds0_UpdateMagSensitivity(lsm9ds0_t *dev, uint8_t full_scale);

/* Exported functions ------------------------------------------------------- */
lsm9ds0_Result_t lsm9ds0_Init(lsm9ds0_t *dev)
{
	uint8_t reg_val, full_scale;
	lsm9ds0_Result_t result;

	if(dev == NULL) return LSM9DS0_NULL_PTR_ERR;

	/* Read WHO_AM_I_G register to check communication with gyroscope */
	result = lsm9ds0_GetWhoAmIG(dev, &reg_val);
	if(result != LSM9DS0_OK) return result;

	/* Check value of the WHO_AM_I_G register */
	if(reg_val != LSM9DS0_WHO_AM_I_G_VALUE) {
		LOG("lsm9ds0_init: Wrong WHO_AM_I_G value.\r\n");
		LOG("lsm9ds0_init: Read: %u (0x%x). Expected %u (0x%x).\r\n",
				reg_val, reg_val, LSM9DS0_WHO_AM_I_G_VALUE,
				LSM9DS0_WHO_AM_I_G_VALUE);
		return LSM9DS0_WRONG_WHO_AM_I_ERR;
	}

	/* Read WHO_AM_I_XM register to check communication with accelerometer
	 * and magnetic sensor */
	result = lsm9ds0_GetWhoAmIXM(dev, &reg_val);
	if(result != LSM9DS0_OK) return result;

	/* Check value of the WHO_AM_I_XM register */
	if(reg_val != LSM9DS0_WHO_AM_I_XM_VALUE) {
		LOG("lsm9ds0_init: Wrong WHO_AM_I_XM value.\r\n");
		LOG("lsm9ds0_init: Read: %u (0x%x). Expected %u (0x%x).\r\n",
				reg_val, reg_val, LSM9DS0_WHO_AM_I_XM_VALUE,
				LSM9DS0_WHO_AM_I_XM_VALUE);
		return LSM9DS0_WRONG_WHO_AM_I_ERR;
	}

	/* Read current gyroscope sensitivity */
	result = lsm9ds0_GetParam(dev, LSM9DS0_GYRO_FS, &full_scale);
	if(result != LSM9DS0_OK) return result;
	lsm9ds0_UpdateGyroSensitivity(dev, full_scale);

	/* Read current accelerometer sensitivity */
	result = lsm9ds0_GetParam(dev, LSM9DS0_ACCEL_FS, &full_scale);
	if(result != LSM9DS0_OK) return result;
	lsm9ds0_UpdateAccelSensitivity(dev, full_scale);

	/* Read current magnetometer sensitivity */
	result = lsm9ds0_GetParam(dev, LSM9DS0_MAG_FS, &full_scale);
	if(result != LSM9DS0_OK) return result;
	lsm9ds0_UpdateMagSensitivity(dev, full_scale);

	return LSM9DS0_OK;
}

lsm9ds0_Result_t lsm9ds0_GetWhoAmIG(lsm9ds0_t *dev, uint8_t *val)
{
	bool result;

	if(dev == NULL || val == NULL) return LSM9DS0_NULL_PTR_ERR;
	if(dev->bus_read == NULL) return LSM9DS0_NULL_PTR_ERR;

	/* Call read function provided by low-level driver */
	result = dev->bus_read(dev->g_id, LSM9DS0_WHO_AM_I_G, val, 1, false);
	if(!result) return LSM9DS0_BUS_ERR;

	return LSM9DS0_OK;
}

lsm9ds0_Result_t lsm9ds0_GetWhoAmIXM(lsm9ds0_t *dev, uint8_t *val)
{
	bool result;

	if(dev == NULL || val == NULL) return LSM9DS0_NULL_PTR_ERR;
	if(dev->bus_read == NULL) return LSM9DS0_NULL_PTR_ERR;

	/* Call read function provided by low-level driver */
	result = dev->bus_read(dev->xm_id, LSM9DS0_WHO_AM_I_XM, val, 1, false);
	if(!result) return LSM9DS0_BUS_ERR;

	return LSM9DS0_OK;
}

lsm9ds0_Result_t lsm9ds0_GetParam(lsm9ds0_t *dev, lsm9ds0_Param_t param, uint8_t *val)
{
	lsm9ds0_ParamDef_t p;

	/* Check if such param exists */
	if(param >= _LSM9DS0_PARAM_END) return LSM9DS0_UNKNOWN_PARAM;

	p = param_def[param];
	return lsm9ds0_GetRegBits(dev, p.interface, p.reg, p.mask, p.pos, val);
}

lsm9ds0_Result_t lsm9ds0_SetParam(lsm9ds0_t *dev, lsm9ds0_Param_t param, uint8_t val)
{
	lsm9ds0_ParamDef_t p;

	/* Check if such param exists */
	if(param >= _LSM9DS0_PARAM_END) return LSM9DS0_UNKNOWN_PARAM;

	/* Check if changing this param requires any additional actions */
	if(param == LSM9DS0_GYRO_FS) {
		/* Changing gyroscope full-scale requires updating sensitivity */
		lsm9ds0_UpdateGyroSensitivity(dev, val);

	} else if(param == LSM9DS0_ACCEL_FS) {
		/* Changing accelerometer full-scale requires updating sensitivity */
		lsm9ds0_UpdateAccelSensitivity(dev, val);

	} else if(param == LSM9DS0_MAG_FS) {
		/* Changing magnetometer full-scale requires updating sensitivity */
		lsm9ds0_UpdateMagSensitivity(dev, val);
	}

	p = param_def[param];
	return lsm9ds0_UpdateRegBits(dev, p.interface, p.reg, p.mask, p.pos, val);
}

lsm9ds0_Result_t lsm9ds0_GetGyroDataRaw(lsm9ds0_t *dev, int16_t *x, int16_t *y, int16_t *z)
{
	bool result;
	uint8_t buffer[6];

	if(dev == NULL) return LSM9DS0_NULL_PTR_ERR;
	if(dev->bus_read == NULL) return LSM9DS0_NULL_PTR_ERR;

	/* Read raw data */
	result = dev->bus_read(dev->g_id, LSM9DS0_OUT_X_L_G, buffer, 6, true);
	if(!result) return LSM9DS0_BUS_ERR;

	*x = (int16_t)((((uint16_t)buffer[1]) << 8) | buffer[0]);
	*y = (int16_t)((((uint16_t)buffer[3]) << 8) | buffer[2]);
	*z = (int16_t)((((uint16_t)buffer[5]) << 8) | buffer[4]);

	return LSM9DS0_OK;
}

lsm9ds0_Result_t lsm9ds0_GetGyroData(lsm9ds0_t *dev, float *x, float *y, float *z)
{
	lsm9ds0_Result_t result;
	int16_t x_raw, y_raw, z_raw;

	/* Read raw data */
	result = lsm9ds0_GetGyroDataRaw(dev, &x_raw, &y_raw, &z_raw);
	if(result != LSM9DS0_OK) return result;

	/* Scale by current sensitivity */
	*x = (float)x_raw * dev->gyro_sensitivity / 1000000;
	*y = (float)y_raw * dev->gyro_sensitivity / 1000000;
	*z = (float)z_raw * dev->gyro_sensitivity / 1000000;

	return LSM9DS0_OK;
}

lsm9ds0_Result_t lsm9ds0_GetAccelDataRaw(lsm9ds0_t *dev, int16_t *x, int16_t *y, int16_t *z)
{
	bool result;
	uint8_t buffer[6];

	if(dev == NULL) return LSM9DS0_NULL_PTR_ERR;
	if(dev->bus_read == NULL) return LSM9DS0_NULL_PTR_ERR;

	/* Read raw data */
	result = dev->bus_read(dev->xm_id, LSM9DS0_OUT_X_L_A, buffer, 6, true);
	if(!result) return LSM9DS0_BUS_ERR;

	*x = (int16_t)((((uint16_t)buffer[1]) << 8) | buffer[0]);
	*y = (int16_t)((((uint16_t)buffer[3]) << 8) | buffer[2]);
	*z = (int16_t)((((uint16_t)buffer[5]) << 8) | buffer[4]);

	return LSM9DS0_OK;
}

lsm9ds0_Result_t lsm9ds0_GetAccelData(lsm9ds0_t *dev, float *x, float *y, float *z)
{
	lsm9ds0_Result_t result;
	int16_t x_raw, y_raw, z_raw;

	/* Read raw data */
	result = lsm9ds0_GetAccelDataRaw(dev, &x_raw, &y_raw, &z_raw);
	if(result != LSM9DS0_OK) return result;

	/* Scale by current sensitivity */
	*x = (float)x_raw * dev->accel_sensitivity / 1000000;
	*y = (float)y_raw * dev->accel_sensitivity / 1000000;
	*z = (float)z_raw * dev->accel_sensitivity / 1000000;

	return LSM9DS0_OK;
}

lsm9ds0_Result_t lsm9ds0_GetMagDataRaw(lsm9ds0_t *dev, int16_t *x, int16_t *y, int16_t *z)
{
	bool result;
	uint8_t buffer[6];

	if(dev == NULL) return LSM9DS0_NULL_PTR_ERR;
	if(dev->bus_read == NULL) return LSM9DS0_NULL_PTR_ERR;

	/* Read raw data */
	result = dev->bus_read(dev->xm_id, LSM9DS0_OUT_X_L_M, buffer, 6, true);
	if(!result) return LSM9DS0_BUS_ERR;

	*x = (int16_t)((((uint16_t)buffer[1]) << 8) | buffer[0]);
	*y = (int16_t)((((uint16_t)buffer[3]) << 8) | buffer[2]);
	*z = (int16_t)((((uint16_t)buffer[5]) << 8) | buffer[4]);

	return LSM9DS0_OK;
}

lsm9ds0_Result_t lsm9ds0_GetMagData(lsm9ds0_t *dev, float *x, float *y, float *z)
{
	lsm9ds0_Result_t result;
	int16_t x_raw, y_raw, z_raw;

	/* Read raw data */
	result = lsm9ds0_GetMagDataRaw(dev, &x_raw, &y_raw, &z_raw);
	if(result != LSM9DS0_OK) return result;

	/* Scale by current sensitivity */
	*x = (float)x_raw * dev->mag_sensitivity / 1000000;
	*y = (float)y_raw * dev->mag_sensitivity / 1000000;
	*z = (float)z_raw * dev->mag_sensitivity / 1000000;

	return LSM9DS0_OK;
}

/* Private functions -------------------------------------------------------- */
static lsm9ds0_Result_t lsm9ds0_GetRegBits(lsm9ds0_t *dev, uint8_t interface,
		uint8_t reg, uint8_t mask, uint8_t pos, uint8_t *val)
{
	bool result;
	uint8_t id;

	if(dev == NULL || val == NULL) return LSM9DS0_NULL_PTR_ERR;
	if(dev->bus_read == NULL) return LSM9DS0_NULL_PTR_ERR;

	/* Find out interface ID */
	if(interface == LSM9DS0_INTERFACE_G) {
		id = dev->g_id;
	} else if(interface == LSM9DS0_INTERFACE_XM) {
		id = dev->xm_id;
	} else {
		return LSM9DS0_ERR;
	}

	/* Call read function provided by low-level driver */
	result = dev->bus_read(id, reg, val, 1, false);
	if(!result) return LSM9DS0_BUS_ERR;

	/* Extract param bits */
	*val = (*val & mask) >> pos;

	return LSM9DS0_OK;
}

static lsm9ds0_Result_t lsm9ds0_UpdateRegBits(lsm9ds0_t *dev, uint8_t interface,
		uint8_t reg, uint8_t mask, uint8_t pos, uint8_t val)
{
	bool result;
	uint8_t id, reg_val;

	if(dev == NULL) return LSM9DS0_NULL_PTR_ERR;
	if(dev->bus_write == NULL || dev->bus_read == NULL) return LSM9DS0_NULL_PTR_ERR;

	/* Find out interface ID */
	if(interface == LSM9DS0_INTERFACE_G) {
		id = dev->g_id;
	} else if(interface == LSM9DS0_INTERFACE_XM) {
		id = dev->xm_id;
	} else {
		return LSM9DS0_ERR;
	}

	/* Read current register value */
	result = dev->bus_read(id, reg, &reg_val, 1, false);
	if(!result) return LSM9DS0_BUS_ERR;

	/* Clear old param value */
	reg_val &= ~mask;

	/* Set new param value */
	reg_val |= (val << pos) & mask;

	/* Write new register value */
	result = dev->bus_write(id, reg, &reg_val, 1, false);
	if(!result) return LSM9DS0_BUS_ERR;

	return LSM9DS0_OK;
}

static void lsm9ds0_UpdateGyroSensitivity(lsm9ds0_t *dev, uint8_t full_scale)
{
	switch(full_scale) {
	case LSM9DS0_GYRO_FS_245_DPS:
		dev->gyro_sensitivity = LSM9DS0_GYRO_FS_245_DPS_SENS;
		break;
	case LSM9DS0_GYRO_FS_500_DPS:
		dev->gyro_sensitivity = LSM9DS0_GYRO_FS_500_DPS_SENS;
		break;
	case LSM9DS0_GYRO_FS_2000_DPS:
		dev->gyro_sensitivity = LSM9DS0_GYRO_FS_2000_DPS_SENS;
		break;
	default:
		break;
	}
}

static void lsm9ds0_UpdateAccelSensitivity(lsm9ds0_t *dev, uint8_t full_scale)
{
	switch(full_scale) {
	case LSM9DS0_ACCEL_FS_2_G:
		dev->accel_sensitivity = LSM9DS0_ACCEL_FS_2_G_SENS;
		break;
	case LSM9DS0_ACCEL_FS_4_G:
		dev->accel_sensitivity = LSM9DS0_ACCEL_FS_4_G_SENS;
		break;
	case LSM9DS0_ACCEL_FS_6_G:
		dev->accel_sensitivity = LSM9DS0_ACCEL_FS_6_G_SENS;
		break;
	case LSM9DS0_ACCEL_FS_8_G:
		dev->accel_sensitivity = LSM9DS0_ACCEL_FS_8_G_SENS;
		break;
	case LSM9DS0_ACCEL_FS_16_G:
		dev->accel_sensitivity = LSM9DS0_ACCEL_FS_16_G_SENS;
		break;
	default:
		break;
	}
}

static void lsm9ds0_UpdateMagSensitivity(lsm9ds0_t *dev, uint8_t full_scale)
{
	switch(full_scale) {
	case LSM9DS0_MAG_FS_2_GAUSS:
		dev->mag_sensitivity = LSM9DS0_MAG_FS_2_GAUSS_SENS;
		break;
	case LSM9DS0_MAG_FS_4_GAUSS:
		dev->mag_sensitivity = LSM9DS0_MAG_FS_4_GAUSS_SENS;
		break;
	case LSM9DS0_MAG_FS_8_GAUSS:
		dev->mag_sensitivity = LSM9DS0_MAG_FS_8_GAUSS_SENS;
		break;
	case LSM9DS0_MAG_FS_12_GAUSS:
		dev->mag_sensitivity = LSM9DS0_MAG_FS_12_GAUSS_SENS;
		break;
	default:
		break;
	}
}
