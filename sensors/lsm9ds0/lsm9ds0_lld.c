/*
 * lsm9ds0_lld.c
 *
 *  Created on: 30.10.2016
 *      Author: krystian
 */

#include <string.h>
#include "lsm9ds0_lld.h"
#include "spi_lib.h"
#include "bsp.h"

/* Private definitions ------------------------------------------------------ */
#define LSM9DS0_READ_BIT					((uint8_t)(1 << 7))
#define LSM9DS0_AUTO_INCREMENT_BIT			((uint8_t)(1 << 6))

#define LSM9DS0_GYRO_ID						((uint8_t)0)
#define LSM9DS0_ACCEL_MAG_ID				((uint8_t)1)

/* Private functions -------------------------------------------------------- */
/**
 * Selects/deselects device by steering chip select pin.
 * @param select true if device is to be selected. Otherwise false.
 * @param dev_id ID of the device to select/deselect.
 * @return Returns false if ID is unknown. Otherwise returns true.
 */
bool lsm9ds0_lld_chip_select(bool select, uint8_t dev_id);

/* Exported functions ------------------------------------------------------- */
void lsm9ds0_lld_init(lsm9ds0_t *dev)
{
	if(dev == NULL) return;

	dev->bus_read = lsm9ds0_lld_read;
	dev->bus_write = lsm9ds0_lld_write;
	dev->g_id = LSM9DS0_GYRO_ID;
	dev->xm_id = LSM9DS0_ACCEL_MAG_ID;
}

bool lsm9ds0_lld_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_val,
		uint8_t length, bool auto_incr)
{
	uint8_t rxBuffer[length + 1], txBuffer[length + 1], i;

	/* Make sure RW bit is set */
	reg_addr |= LSM9DS0_READ_BIT;

	/* Set/clear MS (address auto-increment) bit */
	if(auto_incr) {
		reg_addr |= LSM9DS0_AUTO_INCREMENT_BIT;
	} else {
		reg_addr &= ~LSM9DS0_AUTO_INCREMENT_BIT;
	}

	/* Fill-in TX buffer */
	memset(txBuffer, 0, sizeof(txBuffer));
	txBuffer[0] = reg_addr;

	// Exchange data
	if(!lsm9ds0_lld_chip_select(true, dev_id)) return false;
	spi_readWrite(rxBuffer, txBuffer, length + 1);
	if(!lsm9ds0_lld_chip_select(false, dev_id)) return false;

	/* Copy received data to the destination buffer */
	for(i = 0; i < length; i++) {
		reg_val[i] = rxBuffer[i + 1];
	}

	return true;
}

bool lsm9ds0_lld_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_val,
		uint8_t length, bool auto_incr)
{
	uint8_t txBuffer[length + 1], i;

	/* Make sure RW bit is cleared */
	reg_addr &= ~LSM9DS0_READ_BIT;

	/* Set/clear MS (address auto-increment) bit */
	if(auto_incr) {
		reg_addr |= LSM9DS0_AUTO_INCREMENT_BIT;
	} else {
		reg_addr &= ~LSM9DS0_AUTO_INCREMENT_BIT;
	}

	/* Fill-in TX buffer */
	txBuffer[0] = reg_addr;
	for(i = 0; i < length; i++) {
		txBuffer[i + 1] = reg_val[i];
	}

	// Exchange data
	if(!lsm9ds0_lld_chip_select(true, dev_id)) return false;
	spi_readWrite(NULL, txBuffer, length + 1);
	if(!lsm9ds0_lld_chip_select(false, dev_id)) return false;

	return true;
}

/* Private functions -------------------------------------------------------- */
bool lsm9ds0_lld_chip_select(bool select, uint8_t dev_id)
{
	switch(dev_id) {
	case LSM9DS0_GYRO_ID:
		lsm9ds0_g_chip_select(select);
		break;
	case LSM9DS0_ACCEL_MAG_ID:
		lsmds0_xm_chip_select(select);
		break;
	default:
		/* Unknown device ID */
		return false;
	}

	return true;
}
