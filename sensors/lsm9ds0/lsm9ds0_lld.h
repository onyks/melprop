/*
 * lsm9ds0_lld.h
 *
 *  Created on: 30.10.2016
 *      Author: krystian
 */

#ifndef LSM9DS0_LLD_H_
#define LSM9DS0_LLD_H_

#include <stdbool.h>
#include "lsm9ds0.h"

/* Exported functions ------------------------------------------------------- */
/**
 * Initializes low-level driver and the instance of the sensor
 * @param dev Pointer to the instance of the sensor
 */
void lsm9ds0_lld_init(lsm9ds0_t *dev);

/**
 * Read data from the sensor
 * @param dev_id Sensor ID assigned
 * @param reg_addr Address of the first register to read from
 * @param reg_val Pointer to the buffer where read data will be stored
 * @param length Number of bytes to read
 * @param auto_incr If set to true and reading more than one byte,
 * value of the reg_addr will be incremented after each byte
 * @return Result of the operation
 * @retval true Operation was successful
 * @retval false Operation failed
 */
bool lsm9ds0_lld_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_val,
		uint8_t length, bool auto_incr);

/**
 * Writes data to the sensor
 * @param dev_id Sensor ID assigned
 * @param reg_addr Address of the first register to write to
 * @param reg_val Pointer to the buffer storing data to write
 * @param length Number of bytes to write
 * @param auto_incr If set to true and reading more than one byte,
 * value of the reg_addr will be incremented after each byte
 * @return Result of the operation
 * @retval true Operation was successful
 * @retval false Operation failed
 */
bool lsm9ds0_lld_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_val,
		uint8_t length, bool auto_incr);

#endif /* LSM9DS0_LLD_H_ */
