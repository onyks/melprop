# List of all the sensors related files.
SENSORSSRC = ${CHIBIOS}/sensors/bmp280/bmp280.c \
             ${CHIBIOS}/sensors/bmp280/bmp280_lld.c \
             ${CHIBIOS}/sensors/lsm9ds0/lsm9ds0.c \
             ${CHIBIOS}/sensors/lsm9ds0/lsm9ds0_lld.c \
             ${CHIBIOS}/sensors/sensor_hub.c

# Required include directories
SENSORSINC = ${CHIBIOS}/sensors \
             ${CHIBIOS}/sensors/bmp280 \
             ${CHIBIOS}/sensors/lsm9ds0
