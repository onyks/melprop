/*
 * extconf.h
 *
 *  Created on: 06.11.2016
 *      Author: krystian
 */

#ifndef EXTCONF_H_
#define EXTCONF_H_

#include "ch.h"
#include "hal.h"

/* Exported functions ------------------------------------------------------ */
/**
 * Configures External Interrupt Driver.
 *
 * See source file for configuration of specific EXTI channels and definitions
 * of callback functions.
 */
void extConfigure(void);

#endif /* EXTCONF_H_ */
