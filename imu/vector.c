/*
 * vector.c
 *
 *  Created on: 11 lut 2016
 *      Author: krystian
 */
#include "vector.h"
#include "quaternion.h"

/* Exported functions ------------------------------------------------------ */
vector3f_t vector_rotate(vector3f_t v, quat_t Q)
{
	quat_t V, V_rotated;
	vector3f_t v_rotated;

	// Make quaternion out of v vector
	V.w = 0;
	V.x = v.x;
	V.y = v.y;
	V.z = v.z;

	// Formula used for rotation: V_rotated = QVQ^(-1)
	V_rotated = quat_multiply(Q, V);
	V_rotated = quat_multiply(V_rotated, quat_conjugate(Q));

	// Get the resulting vector
	v_rotated.x = V_rotated.x;
	v_rotated.y = V_rotated.y;
	v_rotated.z = V_rotated.z;

	return v_rotated;
}

vector3f_t vector_normalize(vector3f_t v)
{
	float magnitude;
	vector3f_t normalized;

	magnitude = sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);

	normalized.x = v.x/magnitude;
	normalized.y = v.y/magnitude;
	normalized.z = v.z/magnitude;

	return normalized;
}
