/*
 * quaternion.h
 *
 *  Created on: 11 lis 2015
 *      Author: krystian
 */

#ifndef QUATERNION_H_
#define QUATERNION_H_

#include "math.h"
#include "vector.h"

/* Exported types ---------------------------------------------------------- */
typedef struct quat {
	float w;
	float x;
	float y;
	float z;
} quat_t;

/* Exported functions ------------------------------------------------------ */
quat_t quat_normalize(quat_t q);
quat_t quat_conjugate(quat_t q);
quat_t quat_multiply(quat_t a, quat_t b);
void quat_toAxisAngle(quat_t quat, vector3f_t *axis, float *angle);
quat_t quat_fromAxisAngle(vector3f_t axis, float angle);
quat_t quat_fromEulerAngles(float yaw, float pitch, float roll);

#endif /* QUATERNION_H_ */
