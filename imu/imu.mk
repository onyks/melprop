# List of all the imu related files.
IMUSRC = ${CHIBIOS}/imu/vector.c
IMUSRC += ${CHIBIOS}/imu/quaternion.c

# Required include directories
IMUINC = ${CHIBIOS}/imu
