/*
 * quaternions.c
 *
 *  Created on: 11 lis 2015
 *      Author: krystian
 */
#include "quaternion.h"

/* Exported functions ------------------------------------------------------ */
quat_t quat_normalize(quat_t q)
{
	quat_t normalized;
	float magnitude;

	magnitude = sqrtf(q.w*q.w + q.x*q.x + q.y*q.y + q.z*q.z);

	normalized.w = q.w/magnitude;
	normalized.x = q.x/magnitude;
	normalized.y = q.y/magnitude;
	normalized.z = q.z/magnitude;

	return normalized;
}

quat_t quat_conjugate(quat_t q)
{
	quat_t conjugate;

	conjugate.w = q.w;
	conjugate.x = -q.x;
	conjugate.y = -q.y;
	conjugate.z = -q.z;

	return conjugate;
}

quat_t quat_multiply(quat_t a, quat_t b)
{
	quat_t product;

	product.w = a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z;
	product.x = a.y*b.z - a.z*b.y + a.x*b.w + a.w*b.x;
	product.y = a.z*b.x - a.x*b.z + a.y*b.w + a.w*b.y;
	product.z = a.x*b.y - a.y*b.x + a.z*b.w + a.w*b.z;

	return product;
}

void quat_toAxisAngle(quat_t quat, vector3f_t *axis, float *angle)
{
	float tmp;

	*angle = 2*acosf(quat.w);

	if(*angle >= 0.001f) {

		tmp = sinf(*angle/2);

		axis->x = quat.x/tmp;
		axis->y = quat.y/tmp;
		axis->z = quat.z/tmp;

	} else {

		axis->x = 0;
		axis->y = 0;
		axis->z = 0;
	}
}

quat_t quat_fromAxisAngle(vector3f_t axis, float angle)
{
	quat_t q;
	float tmp;

	q.w = cosf(angle/2);

	tmp = sinf(angle/2);
	q.x = axis.x*tmp;
	q.y = axis.y*tmp;
	q.z = axis.z*tmp;

	return q;
}

quat_t quat_fromEulerAngles(float yaw, float pitch, float roll)
{
	quat_t Y, P, R, tmp, result;

	Y.w = cosf(yaw/2);
	Y.x = 0;
	Y.y = sinf(yaw/2);
	Y.z = 0;

	P.w = cosf(pitch/2);
	P.x = sinf(pitch/2);
	P.y = 0;
	P.z = 0;

	R.w = cosf(roll/2);
	R.x = 0;
	R.y = 0;
	R.z = sinf(roll/2);

	tmp = quat_multiply(Y, P);

	result = quat_multiply(tmp, R);

	return result;
}
