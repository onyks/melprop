/*
 * vector.h
 *
 *  Created on: 13 lis 2015
 *      Author: krystian
 */

#ifndef VECTOR_H_
#define VECTOR_H_

/* External types ---------------------------------------------------------- */
typedef struct quat quat_t;

/* Exported types ---------------------------------------------------------- */
typedef struct {
	float x;
	float y;
	float z;
} vector3f_t;

/* Exported functions ------------------------------------------------------ */
vector3f_t vector_rotate(vector3f_t vector, quat_t quat);
vector3f_t vector_normalize(vector3f_t v);

#endif /* VECTOR_H_ */
