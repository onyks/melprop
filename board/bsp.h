/*
 * bsp.h
 *
 *  Created on: 29.10.2016
 *      Author: krystian
 */

#ifndef BOARD_BSP_H_
#define BOARD_BSP_H_

#include <stdbool.h>

/* Exported functions ------------------------------------------------------ */
/**
 * Controls SPI chip select pin of the BMX055's accelerometer
 * @param select If true, BMX055's accelerometer is selected.
 * If false, sensor is deselected.
 */
void bmx055_acc_chip_select(bool select);

/**
 * Controls SPI chip select pin of the BMX055's gyroscope
 * @param select If true, BMX055's gyroscope is selected.
 * If false, sensor is deselected.
 */
void bmx055_gyro_chip_select(bool select);

/**
 * Controls SPI chip select pin of the BMX055's magnetic sensor
 * @param select If true, BMX055's magnetic sensor is selected.
 * If false, sensor is deselected.
 */
void bmx055_mag_chip_select(bool select);

/**
 * Controls SPI chip select pin of the BMP280 sensor
 * @param select If true, BMP280 is selected. If false, sensor is deselected.
 */
void bmp280_chip_select(bool select);

/**
 * Controls SPI chip select pin of the LSM9DS0's gyroscope
 * @param select If true, LSM9DS0's gyroscope is selected.
 * If false, sensor is deselected.
 */
void lsm9ds0_g_chip_select(bool select);

/**
 * Controls SPI chip select pin of the LSM9DS0's magnetic sensor and accelerometer
 * @param select If true, LSM9DS0's magnetic sensor and accelerometer are selected.
 * If false, sensors are deselected.
 */
void lsmds0_xm_chip_select(bool select);

#endif /* BOARD_BSP_H_ */
