# List of all the board related files.
BOARDSRC = ${CHIBIOS}/board/board.c \
           ${CHIBIOS}/board/bsp.c

# Required include directories
BOARDINC = ${CHIBIOS}/board
