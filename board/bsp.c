/*
 * bsp.c
 *
 *  Created on: 29.10.2016
 *      Author: krystian
 */

#include "bsp.h"
#include "hal.h"

/* Private definitions ----------------------------------------------------- */
/* BMX055 */
#define BMX055_ACC_CS_PORT			GPIOB
#define BMX055_ACC_CS_PAD			GPIOB_BMX055_CSB1
#define BMX055_GYRO_CS_PORT			GPIOB
#define BMX055_GYRO_CS_PAD			GPIOB_BMX055_CSB2
#define BMX055_MAG_CS_PORT			GPIOB
#define BMX055_MAG_CS_PAD			GPIOB_BMX055_CSB3

/* BMP280 */
#define BMP280_CS_PORT				GPIOA
#define BMP280_CS_PAD				GPIOA_BMP280_CSB

/* LSM9DS0 */
#define LSM9DS0_G_CS_PORT			GPIOA
#define LSM9DS0_G_CS_PAD			GPIOA_LSM9DS0_CS_G
#define LSM9DS0_XM_CS_PORT			GPIOA
#define LSM9DS0_XM_CS_PAD			GPIOA_LSM9DS0_CS_XM

/* Exported functions ------------------------------------------------------ */
void bmx055_acc_chip_select(bool select)
{
	if(select) {
		palClearPad(BMX055_ACC_CS_PORT, BMX055_ACC_CS_PAD);
	} else {
		palSetPad(BMX055_ACC_CS_PORT, BMX055_ACC_CS_PAD);
	}
}

void bmx055_gyro_chip_select(bool select)
{
	if(select) {
		palClearPad(BMX055_GYRO_CS_PORT, BMX055_GYRO_CS_PAD);
	} else {
		palSetPad(BMX055_GYRO_CS_PORT, BMX055_GYRO_CS_PAD);
	}
}

void bmx055_mag_chip_select(bool select)
{
	if(select) {
		palClearPad(BMX055_MAG_CS_PORT, BMX055_MAG_CS_PAD);
	} else {
		palSetPad(BMX055_MAG_CS_PORT, BMX055_MAG_CS_PAD);
	}
}

void bmp280_chip_select(bool select)
{
	if(select) {
		palClearPad(BMP280_CS_PORT, BMP280_CS_PAD);
	} else {
		palSetPad(BMP280_CS_PORT, BMP280_CS_PAD);
	}
}

void lsm9ds0_g_chip_select(bool select)
{
	if(select) {
		palClearPad(LSM9DS0_G_CS_PORT, LSM9DS0_G_CS_PAD);
	} else {
		palSetPad(LSM9DS0_G_CS_PORT, LSM9DS0_G_CS_PAD);
	}
}

void lsmds0_xm_chip_select(bool select)
{
	if(select) {
		palClearPad(LSM9DS0_XM_CS_PORT, LSM9DS0_XM_CS_PAD);
	} else {
		palSetPad(LSM9DS0_XM_CS_PORT, LSM9DS0_XM_CS_PAD);
	}
}
