/*
 * led.h
 *
 *  Created on: 29.10.2016
 *      Author: krystian
 */

#ifndef BOARD_LED_H_
#define BOARD_LED_H_

#include "hal.h"

/* Exported definitions ----------------------------------------------------- */
/* LED1 */
#define LED1_PIN				GPIOD_LED1
#define LED1_PORT				GPIOD
#define LED1_ON()				palSetPad(LED1_PORT, LED1_PIN)
#define LED1_OFF()				palClearPad(LED1_PORT, LED1_PIN)
#define LED1_TOGGLE()			palTogglePad(LED1_PORT, LED1_PIN)

/* LED2 */
#define LED2_PIN				GPIOD_LED2
#define LED2_PORT				GPIOD
#define LED2_ON()				palSetPad(LED2_PORT, LED2_PIN)
#define LED2_OFF()				palClearPad(LED2_PORT, LED2_PIN)
#define LED2_TOGGLE()			palTogglePad(LED2_PORT, LED2_PIN)

/* LED3 */
#define LED3_PIN				GPIOD_LED3
#define LED3_PORT				GPIOD
#define LED3_ON()				palSetPad(LED3_PORT, LED3_PIN)
#define LED3_OFF()				palClearPad(LED3_PORT, LED3_PIN)
#define LED3_TOGGLE()			palTogglePad(LED3_PORT, LED3_PIN)

/* LED4 */
#define LED4_PIN				GPIOD_LED4
#define LED4_PORT				GPIOD
#define LED4_ON()				palSetPad(LED4_PORT, LED4_PIN)
#define LED4_OFF()				palClearPad(LED4_PORT, LED4_PIN)
#define LED4_TOGGLE()			palTogglePad(LED4_PORT, LED4_PIN)

#endif /* BOARD_LED_H_ */
