/*
 * extconf.c
 *
 *  Created on: 06.11.2016
 *      Author: krystian
 */

#include "extconf.h"
#include "sensor_hub.h"

/* EXTI Options ------------------------------------------------------------ */
#define AUTOSTART				EXT_CH_MODE_AUTOSTART

/* Edge */
#define RISING_EDGE				EXT_CH_MODE_RISING_EDGE
#define FALLING_EDGE			EXT_CH_MODE_FALLING_EDGE
#define BOTH_EDGES				EXT_CH_MODE_BOTH_EDGES

/* Callback functions ------------------------------------------------------ */
void EXTI1_callback(EXTDriver *extp, expchannel_t channel);
void EXTI2_callback(EXTDriver *extp, expchannel_t channel);
void EXTI9_callback(EXTDriver *extp, expchannel_t channel);

/* EXTI Configuration ------------------------------------------------------ */
static const EXTConfig extConfig = {{
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI0 */
    {AUTOSTART | RISING_EDGE | EXT_MODE_GPIOB, EXTI1_callback}, /* EXTI1 */
    {AUTOSTART | RISING_EDGE | EXT_MODE_GPIOB, EXTI2_callback}, /* EXTI2 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI3 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI4 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI5 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI6 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI7 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI8 */
    {AUTOSTART | RISING_EDGE | EXT_MODE_GPIOE, EXTI9_callback}, /* EXTI9 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI10 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI11 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI12 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI13 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI14 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI15 */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI16 (PVD output) */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI17 (RTC Alarm event) */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI18 (USB OTG FS Wakeup event) */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI19 (Ethernet Wakeup event) */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI20 (USB OTG HS Wakeup event) */
    {EXT_CH_MODE_DISABLED, NULL}, /* EXTI21 (RTC Tamper and TimeStamp events) */
    {EXT_CH_MODE_DISABLED, NULL} /* EXTI22 (RTC Wakeup event) */
}};

/* Exported functions ------------------------------------------------------ */
void extConfigure(void) {
	extStart(&EXTD1, &extConfig);
}

/* Callback functions ------------------------------------------------------ */
void EXTI1_callback(EXTDriver *extp, expchannel_t channel)
{
	(void)extp;
	(void)channel;
	thread_t *thread = SensorHub_GetThreadPtr();

	if(thread != NULL) {
		chSysLockFromISR();
		chEvtSignalI(thread, SENSOR_HUB_EVT_MAG_NEW_DATA);
		chSysUnlockFromISR();
	}
}

void EXTI2_callback(EXTDriver *extp, expchannel_t channel)
{
	(void)extp;
	(void)channel;
	thread_t *thread = SensorHub_GetThreadPtr();

	if(thread != NULL) {
		chSysLockFromISR();
		chEvtSignalI(thread, SENSOR_HUB_EVT_ACCEL_NEW_DATA);
		chSysUnlockFromISR();
	}
}

void EXTI9_callback(EXTDriver *extp, expchannel_t channel)
{
	(void)extp;
	(void)channel;
	thread_t *thread = SensorHub_GetThreadPtr();

	if(thread != NULL) {
		chSysLockFromISR();
		chEvtSignalI(thread, SENSOR_HUB_EVT_GYRO_NEW_DATA);
		chSysUnlockFromISR();
	}
}
