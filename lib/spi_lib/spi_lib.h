/*
 * spi_lib.h
 *
 *  Created on: 17 gru 2015
 *      Author: krystian
 */

#ifndef SPI_LIB_SPI_LIB_H_
#define SPI_LIB_SPI_LIB_H_

#include "ch.h"

/* Exported definitions ---------------------------------------------------- */
/* Exported functions ------------------------------------------------------ */
/**
 * Initialize SPI driver.
 */
void spi_init(void);

/**
 * Exchange data using SPI interface.
 * Make sure to take care of the chip select pins before calling this function.
 * @param inBuff Input buffer. May be NULL when writing only.
 * @param outBuff Output buffer. May be NULL when reading only.
 * @param size Number of bytes to exchange.
 */
void spi_readWrite(uint8_t *rxBuff, uint8_t *txBuff, uint32_t size);

#endif /* SPI_LIB_SPI_LIB_H_ */
