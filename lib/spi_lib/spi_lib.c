/*
 * spi_lib.c
 *
 *  Created on: 17 gru 2015
 *      Author: krystian
 */

#include "spi_lib.h"
#include "hal.h"

/* Private definitions ----------------------------------------------------- */
#define SPI_DRIVER					SPID1

/* Private variables ------------------------------------------------------- */
SPIConfig config;

/* Exported functions ------------------------------------------------------ */
void spi_init(void)
{
	config.end_cb = NULL;
	config.cr1 = SPI_CR1_BR_1 | SPI_CR1_CPHA | SPI_CR1_CPOL;

	// Configure SPI pins
	palSetPadMode(GPIOA, GPIOA_SCK, PAL_MODE_ALTERNATE(5));
	palSetPadMode(GPIOA, GPIOA_MISO, PAL_MODE_ALTERNATE(5));
	palSetPadMode(GPIOA, GPIOA_MOSI, PAL_MODE_ALTERNATE(5));

	spiStart(&SPI_DRIVER, &config);
}

void spi_readWrite(uint8_t *rxBuff, uint8_t *txBuff, uint32_t size)
{
	spiAcquireBus(&SPI_DRIVER);

	spi_init();

	if(txBuff == NULL && rxBuff == NULL) return;

	if(rxBuff == NULL) {
		spiSend(&SPI_DRIVER, size, txBuff);
	} else if(txBuff == NULL) {
		spiReceive(&SPI_DRIVER, size, rxBuff);
	} else {
		spiExchange(&SPI_DRIVER, size, txBuff, rxBuff);
	}

	spiReleaseBus(&SPI_DRIVER);
}
