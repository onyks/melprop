/*
 * shell_commands.c
 *
 *  Created on: 12 lut 2016
 *      Author: krystian
 */

#include "shell_lib.h"
#include "shell_commands.h"

#include "sensor_hub.h"

/* Commands' prototypes ---------------------------------------------------- */
/* Exported variables ------------------------------------------------------ */
ShellCommand shellCommands[] = {
  {NULL, NULL}
};

/* Commands ---------------------------------------------------------------- */
