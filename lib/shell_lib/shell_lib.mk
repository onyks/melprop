# List of all the serial_lib related files.
SHELLLIBSRC = ${CHIBIOS}/lib/shell_lib/shell_commands.c
SHELLLIBSRC += ${CHIBIOS}/lib/shell_lib/shell_lib.c

# Required include directories
SHELLLIBINC = ${CHIBIOS}/lib/shell_lib
