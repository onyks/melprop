/*
 * shell_lib.c
 *
 *  Created on: 19 gru 2015
 *      Author: krystian
 */

#include "shell_lib.h"

/* Private variables ------------------------------------------------------- */
static SerialConfig shell_serialConfig = {.speed = SHELL_SERIAL_SPEED, .cr1 = 0, .cr2 = 0, .cr3 = 0};
static ShellConfig shell_config;
static thread_t *shell_threadPtr;
static THD_WORKING_AREA(shell_threadWA, SHELL_THREAD_WORKING_AREA_SIZE);
static MUTEX_DECL(shell_lock);

/* Exported functions ------------------------------------------------------ */
void shell_init(void) {
	sdStart((SerialDriver *)SHELL_SERIAL, &shell_serialConfig);
	palSetPadMode(GPIOC, GPIOC_TXD, PAL_MODE_ALTERNATE(8));
	palSetPadMode(GPIOC, GPIOC_RXD, PAL_MODE_ALTERNATE(8));

	// Configure shell
	shellInit();
	shell_config.sc_channel = SHELL_SERIAL;
	shell_config.sc_commands = shellCommands;
	shell_threadPtr = shellCreateStatic(&shell_config,
			shell_threadWA,
			SHELL_THREAD_WORKING_AREA_SIZE,
			SHELL_THREAD_PRIORITY);
}

void shell_greatings(void) {
	chMtxLock(&shell_lock);

	chprintf(SHELL_SERIAL, "\r\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	chprintf(SHELL_SERIAL, "Kernel:       %s\r\n", CH_KERNEL_VERSION);
#ifdef PORT_COMPILER_NAME
	chprintf(SHELL_SERIAL, "Compiler:     %s\r\n", PORT_COMPILER_NAME);
#endif
#ifdef __DATE__
#ifdef __TIME__
	chprintf(SHELL_SERIAL, "Build time:   %s%s%s\r\n", __DATE__, " - ", __TIME__);
#endif
#endif
	chprintf(SHELL_SERIAL, "\r\nType 'help' for a list of commands.\r\n\r\n");

	chMtxUnlock(&shell_lock);
}

void shell_log(bool timestamp, char *fmt, ...) {
	va_list argptr;
	RTCDateTime timespec;
	struct tm time;

	chMtxLock(&shell_lock);

	// Print timestamp
	if (timestamp) {
		rtcGetTime(&RTCD1, &timespec);
		rtcConvertDateTimeToStructTm(&timespec, &time, NULL);
		chprintf(SHELL_SERIAL, "[%02u:%02u:%02u] ", time.tm_hour, time.tm_min, time.tm_sec);
	}

	// Print actual message
	va_start(argptr,fmt);
	chvprintf(SHELL_SERIAL, fmt, argptr);
	va_end(argptr);

	chMtxUnlock(&shell_lock);
}
