/*
 * shell_commands.h
 *
 *  Created on: 12 lut 2016
 *      Author: krystian
 */

#ifndef SHELL_COMMANDS_H_
#define SHELL_COMMANDS_H_

#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "shell.h"

/* Exported variables ------------------------------------------------------ */
extern ShellCommand shellCommands[];

#endif /* SHELL_COMMANDS_H_ */
