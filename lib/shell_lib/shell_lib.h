/*
 * shell_lib.h
 *
 *  Created on: 19 gru 2015
 *      Author: krystian
 */

#ifndef SHELL_LIB_H_
#define SHELL_LIB_H_

#include "board.h"
#include "shell_commands.h"

/* Configuration ----------------------------------------------------------- */
#define SHELL_THREAD_WORKING_AREA_SIZE			((size_t)4096)
#define SHELL_THREAD_PRIORITY					((tprio_t)NORMALPRIO)

/* Exported definitions and macros ----------------------------------------- */
#define SHELL_SERIAL							((BaseSequentialStream *)&SD6)
#define SHELL_SERIAL_SPEED						((uint32_t)115200)

#define LOG(...)								shell_log(true, __VA_ARGS__)
#define LOG_RAW(...)							shell_log(false, __VA_ARGS__)

/* Exported functions ------------------------------------------------------ */
void shell_init(void);
void shell_greatings(void);
void shell_log(bool timestamp, char *fmt, ...);

#endif /* SHELL_LIB_H_ */
